import 'package:flutter/material.dart';
import 'package:lectorqr/src/pages/Login/login_es_page.dart';
import 'package:lectorqr/src/pages/Login/resetear_pass_page.dart';
import 'package:lectorqr/src/pages/Registro/sign_up_es_page.dart';
import 'package:lectorqr/src/pages/articulos_page.dart';
import 'package:lectorqr/src/pages/botones_page.dart';
import 'package:lectorqr/src/pages/categoria_filtro_page.dart';
import 'package:lectorqr/src/pages/comentario_page.dart';
import 'package:lectorqr/src/pages/entidades_page.dart';
import 'package:lectorqr/src/pages/maps_museos_page.dart';
import 'package:lectorqr/src/pages/mejores_valorados.dart';
//import 'package:lectorqr/src/pages/login_page.dart';
import 'package:lectorqr/src/pages/principal_page.dart';
//import 'package:lectorqr/src/pages/registro_page.dart';
import 'package:lectorqr/src/pages/scan_page.dart';
import 'package:lectorqr/src/pages/scroll_page.dart';
import 'package:lectorqr/src/pages_en/articles_en_page.dart';
import 'package:lectorqr/src/pages_en/botones_en_page.dart';
import 'package:lectorqr/src/pages_en/categoryFilter_en_page.dart';
import 'package:lectorqr/src/pages_en/entidades_en_page.dart';
import 'package:lectorqr/src/pages_en/login_en/login_en_page.dart';
import 'package:lectorqr/src/pages_en/login_en/resetPass_en_page.dart';
import 'package:lectorqr/src/pages_en/login_en/sign_up_en_page.dart';
import 'package:lectorqr/src/pages_en/scan_en_page.dart';
import 'package:lectorqr/src/pages_en/topRated_en_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    'principal'  : (BuildContext context) => PrincipalPage(),

    // Menues Español e Ingles
    'botones'    : (BuildContext context) => BotonesPage(),
    'buttons': (BuildContext context) => BotonesEnPage(),

    //----------------------------------------------------------
    'articulos'  : (BuildContext context) => ArticulosPage(),
    'Comentario' : (BuildContext context) => ComentarioPage(),
    'Categorias' : (BuildContext context) => CategoriaFiltro(),
    'entidades'  : (BuildContext context) => EntidadesPages(),
    'Scan'       : (BuildContext context) => ScanPage(),
    'Scroll'     : (BuildContext context) => ScrollPage(),
    
    'MapsMuseo'  : (BuildContext context) => MapsMuseos(),
    'mejoresVal'  : (BuildContext context) => MejoresValPage(),

    // español
    'loginEs': (BuildContext context) => LoginEsPage(),
    'singUpEs': (BuildContext context) => SignUpEsPage(),
    'resetearPassEs': (BuildContext context) => ResetearPassEsPage(),


    //---------------------------------------------------------
    //Rutas English
   
    'museums': (BuildContext context) => EntidadesEnPages(),
    'articles': (BuildContext context) => ArticlesEnglish(),
    'topRated': (BuildContext context) => TopRatedEnglish(),
    'scanEn': (BuildContext context) => ScanEnglish(),
    'Categories' : (BuildContext context) => CategoryFilter(),
  

    'loginEn': (BuildContext context) => LoginEnglish(),
    'signupEn': (BuildContext context) => SignupEnglish(),
    'resetPassEn': (BuildContext context) => ResetPassEnglish(),
  };
}
