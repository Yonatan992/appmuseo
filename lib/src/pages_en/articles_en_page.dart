import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lectorqr/src/models/articulos_model.dart';
import 'package:lectorqr/src/pages/scope_model/var_global.dart';
import 'package:lectorqr/src/widget/menu_widget_en.dart';

class ArticlesEnglish extends StatefulWidget {
  ArticlesEnglish({Key key}) : super(key: key);

  @override
  _ArticlesEnglishState createState() => _ArticlesEnglishState();
}

class _ArticlesEnglishState extends State<ArticlesEnglish> {
  final Ruta _ruta = Ruta();
  List data;
  ScrollController _scrollController;

   List<Articulo> _articulos = List<Articulo>();
  List<Articulo> _articulosForDisplay = List<Articulo>();

  Future<List<Articulo>> getArticulos() async {
     var response = await http.get(Uri.encodeFull(_ruta.rutaArticulo), headers: {
      "Accept": "application/json",
    });
    
    var articulos = List<Articulo>();
    
    if (response.statusCode == 200) {
      var data = json.decode(response.body);
      var articulosJson = data["data"];
      for (var articulosJson in articulosJson) {
        articulos.add(Articulo.fromJson(articulosJson));
      }
    }
    return articulos;
  }

  @override
  void initState() {
     getArticulos().then((value) {
      setState(() {
        _articulos.addAll(value);
        _articulosForDisplay = _articulos;
      });
    });
    _scrollController = ScrollController();
    super.initState();
  }

   @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  _scrollToTop() {
    _scrollController.animateTo(_scrollController.position.minScrollExtent,
        duration: Duration(milliseconds: 2000), curve: Curves.easeIn);
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return new Scaffold(
      appBar: AppBar(
        elevation: 10,
        shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
        title: Text('List of Articles'),
        
        brightness: Brightness.light,
      ),
      drawer: MenuEnWidget(),
      body: Center(
        child: Container(
          width: size.width * 0.90,
          child: ListView.builder(
                  controller: _scrollController,
                  itemCount: _articulosForDisplay.length +1,
                  itemBuilder: (BuildContext context, int i) {
                    return i == 0 ? _serchArticle() :_listArticles(i-1);
                  }),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: _scrollToTop,
          child: Icon( Icons.arrow_upward),
      )
    );
  }

  Widget _listArticles(int i) {
    return 
    Center(
          child:Card(
              elevation: 20.0,
              shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
              color: Color(0xfff5f5f5).withOpacity(0.8),
              child: Column(
                children: <Widget>[
                    SizedBox(height: 30.0),
                    Text(_articulosForDisplay[i].denominacionEnt == null ? 'Cargando ..' : _articulosForDisplay[i].denominacionEnt,
                        textAlign: TextAlign.center,
                        style:GoogleFonts.montserrat(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))),
                    Text(_articulosForDisplay[i].tituloEn == null ? 'Cargando ..' :_articulosForDisplay[i].tituloEn,
                        textAlign: TextAlign.center,
                        style:GoogleFonts.bevan(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))),
                    SizedBox(height: 30.0),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Container(
                      width: 350,
                      decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Color(0xff2962ff),
                      ),
                            
                      child: Column(
                          children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(topLeft:Radius.circular(10),topRight:Radius.circular(10) ),
                                child: (_articulosForDisplay[i].imagen != null) 
                                  ?FadeInImage(
                                    image: NetworkImage(_articulosForDisplay[i].imagen),
                                    width: MediaQuery.of(context).size.width,
                                    placeholder: AssetImage('assets/giphy.gif'),
                                    fadeInDuration: Duration(milliseconds: 700),
                                    height: 250.0,
                                    fit: BoxFit.cover,
                                  )
                                  : Image(image:AssetImage('assets/no-image.png'),)
                                  ),
                            SizedBox(height: 30.0),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal:20),
                              child: Text(_articulosForDisplay[i].descripcionEn == null ? 'Cargando ..' : _articulosForDisplay[i].descripcionEn,
                                  textAlign: TextAlign.justify,
                                  style:GoogleFonts.lato(textStyle: TextStyle(color: Colors.white, letterSpacing: .5,fontSize: 15))),
                                  ),
                              SizedBox(height: 40.0),
                          ],
                              ),
                            
                          ),
                        ),
                    SizedBox(height: 40.0),
                        
                    Divider()
                ],
              ),
      )
    );
  }

  _serchArticle() {
     final size = MediaQuery.of(context).size;
     return Column(
       
       children: <Widget>[
         SizedBox(
              height: size.height * 0.02,
                                                              
            ),
         Padding(
           padding:const EdgeInsets.all(0.8),
           child: TextField(
             cursorColor: Colors.black,
            
             decoration: InputDecoration(
                icon: Icon(Icons.search),
              border:OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
              ),
              hintText: 'Search articles',
              alignLabelWithHint: true,

              ),
            onChanged:(text){
              text = text.toLowerCase();
              setState(() {
                 _articulosForDisplay = _articulos.where((articulo) {
                  var articuloTitle = articulo.tituloEn.toLowerCase();
                  return articuloTitle.contains(text);
                }).toList();
                
              });
            } ,
           ), 
         ),
        

         Center(
           child: _articulosForDisplay.length == 0
           ? Column(
             children:<Widget>[
               SizedBox(height: size.height * 0.03,),
               CircularProgressIndicator(),
               SizedBox(height: size.height * 0.03,),
               Text('No records found, Loading ...'),
               

             ] )
           :SizedBox(
              height: size.height * 0.01,
                                                              
            ),


         ),
         
       ],
     );
   }

}