import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:lectorqr/src/models/articulos_model.dart';
import 'package:lectorqr/src/pages/scope_model/apiService.dart';
import 'package:lectorqr/src/pages_en/comment_en_page.dart';
import 'package:lectorqr/src/pages_en/comment_update_page.dart';
import 'package:lectorqr/src/utils/dialogs_login_es.dart';
import 'package:lectorqr/src/utils/session_login.dart';
import 'package:rating_bar/rating_bar.dart';


class ShowScanEnglish extends StatefulWidget {
   final String _text;
  ShowScanEnglish(this._text);
  

  @override
   createState() => _ShowScanEnglishState();
}

class _ShowScanEnglishState extends State<ShowScanEnglish> {
int idArticulo;
Map user;

 

var now = new DateTime.now();
var nowActual = new DateTime.now();

// "dd-MM-yyyy hh:mm:ss"
var formatter = new DateFormat("dd-MM-yyyy HH:mm:ss");
var time = new DateFormat("HH:mm:ss");


final _session = Session();
  check() async {
    final data = await _session.getSession();
    if (data != null){
      //Navigator.pushReplacementNamed(context, 'Scan');
      setState(() {
        user = data;
        return user;
      });
      
    } else {
      Navigator.pushReplacementNamed(context, 'loginEn');
    }
  }

 @override
    void initState() { 
      
    super.initState();
    this.check();
    this._showArticulo();
    this._dataArticle(widget._text);
    
    
  }

  @override
  void dispose() {
    super.dispose();
    
  }

   final _apiService = ApiService();
    List<dynamic> comentarios;
    List<dynamic> articulos;
          
  @override
  Widget build(BuildContext context) {




    return Scaffold(
       appBar: AppBar(        
       
        brightness: Brightness.light,
        title: Text('Scanned Item',),
        
      ),
    body: _showArticulo()
      
    );

  }

   
  _dataArticle(qrString) {
  
       return FutureBuilder(
        future:  _apiService.scanArt(context ,qrString),
        builder: (BuildContext context,AsyncSnapshot<Articulo> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final size = MediaQuery.of(context).size;
            return Center(
            child: (snapshot.data == null)
                    ? Container(
                      width: size.width,
                      height: size.height * 0.30,
                      child: Card(
                        elevation: 15,
                      child: Center(child: Column(
                        children: <Widget>[
                          Text('There is no article associated with the QR code',
                              style: TextStyle(
                              fontSize: 15.0)),
                          SizedBox(height:size.height * 0.030),
                          FaIcon(FontAwesomeIcons.exclamationCircle,color:Colors.black.withOpacity(0.6), size: 80,),
                        ],
                      ))),
                    )
            
            :ListView(
              children: <Widget>[
                _listaArticulo(snapshot.data),
              ],
            ),
          );
            
            
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      );
    

  }
   
  _listaArticulo(Articulo articulo) {
      idArticulo = articulo.identificador;
       final size = MediaQuery.of(context).size;
      return Container(
            padding: EdgeInsets.symmetric(horizontal:10.0),
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              children: <Widget>[
                  Card(
                    elevation: 5.0,
                    shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    color: Color(0xfff5f5f5).withOpacity(0.6),
                    child: Column(
                        children: <Widget>[
                          SizedBox(height: 15.0),

                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ClipRRect(
                                    borderRadius:BorderRadius.circular(10.0),
                                    child: Container(
                                      
                                      height: 180,
                                      decoration: BoxDecoration(
                                          borderRadius:BorderRadius.circular(10.0),
                                          gradient: LinearGradient(
                                            begin:Alignment.topCenter,
                                            end:Alignment.bottomCenter,
                                            colors:<Color>[
                                             Color(0xff2962ff),
                                            Color(0xff2962ff).withOpacity(0.8),
                                              
                                            ]
                                          )
                                        ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Column(
                                          children: <Widget>[

                                           

                                             Text(articulo.tituloEs == null ? 'Loading ..' : articulo.tituloEn,
                                                    textAlign: TextAlign.justify,
                                                    overflow: TextOverflow.ellipsis,
                                                    style:GoogleFonts.bevan(textStyle: TextStyle(color: Colors.white, letterSpacing: .5,fontSize: 15))),
                                            SizedBox(height: 10.0),
                                            Text(articulo.subtituloEs == null ? 'Loading ..' : articulo.subtituloEn,
                                                    textAlign: TextAlign.justify,
                                                    overflow: TextOverflow.ellipsis,
                                                    style:GoogleFonts.alegreyaSans(textStyle: TextStyle(color: Colors.white, letterSpacing: .5,fontSize: 15))),

                                          Divider(
                                            color: Colors.white,
                                            height: size.height * 0.020,),
                                          ListTile(
                                            leading: FaIcon(FontAwesomeIcons.home,color:Colors.white.withOpacity(0.8), size: 25,),
                                            title: Text('Entidad: '+ articulo.denominacionEnt == null ? 'Loading ...' : articulo.denominacionEnt,
                                                    
                                                    overflow: TextOverflow.ellipsis,
                                                    style:GoogleFonts.alegreyaSans(textStyle: TextStyle(color: Colors.white, letterSpacing: .5,fontSize: 10))),
                                          ),
                                             
                                          ],
                                        ),
                                      ),
                                    ),
                            ),
                          ),
                              
                          
                             
                          SizedBox(height: 10.0),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Color(0xfff5f5f5),
                              ),
                              child: Column(
                                children: <Widget>[
                                  ClipRRect(
                                     borderRadius: BorderRadius.only(topLeft:Radius.circular(10),topRight:Radius.circular(10) ),
                                    child: (articulo.imagen != null) 
                                      ? FadeInImage(
                                      image: NetworkImage(articulo.imagen),
                                      placeholder: AssetImage('assets/carga.gif'),
                                      fadeInDuration: Duration(milliseconds: 700),
                                      height: 250.0,
                                      width: 360,
                                      fit: BoxFit.cover,
                                    )
                                    : Image(image:AssetImage('assets/no-image.png'),)
                                  ),
                                  Container(
                                    padding: EdgeInsets.symmetric(horizontal:20),
                                    width: 360.0,
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 20.0),
                                        Text(' Category: '+ articulo.denominacionCat == null ? 'Loading ..' : ' Category: ' + articulo.denominacionCatEn,
                                                textAlign: TextAlign.right,
                                                style:GoogleFonts.bevan(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))),
                                        SizedBox(height: 30.0),
                                        Text(articulo.descripcionEs == null ? 'Loading ..' : articulo.descripcionEn,
                                            textAlign: TextAlign.justify,
                                            style:GoogleFonts.openSans(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))),
                                        SizedBox(height: 10.0),
                                        Text(articulo.cuerpoEs == null ? 'Loading ..' : articulo.cuerpoEn,
                                            textAlign: TextAlign.justify,
                                            style:GoogleFonts.openSans(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))),
                                        SizedBox(height: 30.0),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              ),
                          ),

                             
                            
                          ],
                        ),
                      ),
                  Container(
                    decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                    color: Color(0xfff5f5f5).withOpacity(0.8)),
                    child: Card(
                      child: Text('Comments',
                            textAlign: TextAlign.right,
                            overflow: TextOverflow.ellipsis,
                            style:GoogleFonts.abrilFatface(textStyle: TextStyle(color: Colors.blueAccent, letterSpacing: .5,fontSize: 25))),
                    ),
                  ),

                  Container(
                    height: 300,
                    child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                              Flexible(
                                child: Card(
                                  elevation: 15.0,
                                  color: Color(0xfff5f5f5).withOpacity(0.9),
                                  shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0)),
                                  child: ListView.separated(
                                      padding: const EdgeInsets.all(8),
                                      separatorBuilder: (BuildContext context, int index) => const Divider(),
                                      scrollDirection: Axis.vertical,
                                      itemCount: articulo.comments.length,
                                      itemBuilder: (context, index){   
                                        
                                        
                                        return Column(
                                            children: <Widget>[
                                              Container(
                                                child: Card( 
                                                  color:Color(0xffF1F8F8),
                                                  shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(5.0)),
                                                  child: Column(
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                      
                                                          SizedBox(width: size.width * 0.02),
                                                          Icon(Icons.account_circle),
                                                          SizedBox(width: size.width * 0.002),

                                                          Text(articulo.comments[index].apellido == null ? 'Loading ..' : articulo.comments[index].apellido+',',
                                                            textAlign: TextAlign.left,
                                                            overflow: TextOverflow.ellipsis,
                                                            style: TextStyle(
                                                              fontWeight: FontWeight.bold,
                                                              color: Colors.black87,
                                                              fontSize: 10.0)),
                                                              SizedBox(width: size.width * 0.005),

                                                          Text(articulo.comments[index].nombre == null ? 'Loading ..' : articulo.comments[index].nombre,
                                                            textAlign: TextAlign.left,
                                                            overflow: TextOverflow.ellipsis,
                                                            style: TextStyle(
                                                              fontWeight: FontWeight.bold,
                                                              color: Colors.black87,
                                                              fontSize: 10.0)),
                                                            
                                                          RatingBar.readOnly(
                                                                initialRating: double.parse(articulo.comments[index].valoracion),
                                                                isHalfAllowed: true,
                                                                size: size.width * 0.07,
                                                                filledColor: Colors.yellow.withOpacity(0.7),
                                                                halfFilledIcon: Icons.star_half,
                                                                filledIcon: Icons.star,
                                                                emptyIcon: Icons.star_border,
                                                              ),
                                                            
                                                            
                                                          ],
                                                        ),

                                            

                                                      
                                                      Column(
                                                        children: <Widget>[
                                                          Container(
                                                            child: Card(
                                                              color: Colors.white.withOpacity(0.4),
                                                              elevation: 9,
                                                              child: Text(articulo.comments[index].comentario == null ? '' :articulo.comments[index].comentario,
                                                                    textAlign: TextAlign.justify,
                                                                    style:GoogleFonts.openSans(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))),
                                                                ),
                                                              ),

                                                          SizedBox(height: 10.0),
                                                            Text(articulo.comments[index].fecha == null ? 'Loading ..' : ' Publicado: '+ formatter.format( DateTime.parse(articulo.comments[index].fecha)),
                                                            textAlign: TextAlign.left,
                                                            overflow: TextOverflow.ellipsis,
                                                            style:GoogleFonts.openSans(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 10))),
                                                             

                                                          SizedBox(height: 5.0),

                                                          Center(
                                                          child: ClipRRect( 
                                                            borderRadius: BorderRadius.circular(15.0),
                                                            child: (articulo.comments[index].idUser == user['user_id'])
                                                            ? ClipRRect(
                                                                borderRadius: BorderRadius.circular(10.0),
                                                                child: RaisedButton(
                                                                  splashColor: Colors.yellow[200],
                                                                  color: Color(0xFF448aff),
                                                                  onPressed:() => _updateComment(articulo,articulo.comments[index]),
                                                                  textColor: Colors.white,
                                                                  padding: const EdgeInsets.all(10.0),
                                                                  child: Text(
                                                                    'Edit review',
                                                                    style: TextStyle(fontSize: 20)
                                                                  ),
                                                              ),
                                                            )

                                                            : SizedBox(
                                                              width: size.width * 0.05,
                                                              
                                                            ),
                                                          ),
                                                        ),
                                                            ],
                                                          ),
                                                        ],
                                                      )   
                                                ),
                                              ),
                                              SizedBox(height: 15.0),
                                            ],
                                          );
                                          
                                      }),
                                  ),
                                ),
                              ],
                            ),
                  ),
                  SizedBox(height: 10.0),

                  ClipRRect(
                           borderRadius: BorderRadius.circular(10.0),
                           child: RaisedButton(
                            splashColor: Colors.yellow[200],
                            color: Color(0xFF448aff),
                            onPressed: _disableButtom(articulo)== false ? null : () =>_comentarArticulo(articulo),
                            textColor: Colors.white,
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              'Leave a review',
                              style: TextStyle(fontSize: 25)
                            ),
                        ),
                      ),

                SizedBox(height: 20.0),
                ],
              ),
            );


  }

  _comentarArticulo(Articulo articulo) {

     Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => CommentEnglish(articulo: articulo)),
    );

  }

  _showArticulo() {

  
     return  Stack(
      children: <Widget>[
        
         Container(
              child: _dataArticle(widget._text),
            ),

      ],
    );
  }

  void _updateComment(Articulo listArticulo, Comment listComment) {

    

   try{ 
      DateTime _datetimePublic = DateTime.parse(listComment.fecha);
      DateTime _datetimeActual = nowActual;
      Duration difference = _datetimeActual.difference(_datetimePublic);
      print(difference.inMinutes);

      if (difference.inMinutes <= 45) {
         Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) =>CommentUpdatePage(articulo: listArticulo,comment: listComment,)),
      );
        
      } else {
            Dialogs.alert(context,title: "Failed to editing", message:'Exceeded time limit');
      }
      
    }
      on PlatformException catch (e) {
           
            Dialogs.alert(context,title: "Failed", message:e.message);
    }

  }



  _disableButtom(Articulo articulo) {

     bool _bandera = true; 
    for (var i = 0; i < articulo.comments.length;i++ ){
      if (articulo.comments[i].idUser == user['user_id']){
        _bandera = false;

      }
    }

    return _bandera;
  }

  

}