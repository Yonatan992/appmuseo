import 'dart:math';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lectorqr/src/pages/scope_model/AuthApi/auth_api_service.dart';
import 'package:lectorqr/src/utils/dialogs_login_en.dart';
import 'package:lectorqr/src/utils/dialogs_login_es.dart';
import 'package:lectorqr/src/utils/session_login.dart';

class BotonesEnPage extends StatefulWidget {
  @override
  _BotonesEnPageState createState() => _BotonesEnPageState();
}

class _BotonesEnPageState extends State<BotonesEnPage> {
  final _authAPI = AuthAPI();
  Map user;

  @override
  void initState() { 
    super.initState();
    this.check();
    
  }

  final _session = Session();
  check() async {
    final data = await _session.getSession();
    if (data != null){
      //Navigator.pushReplacementNamed(context, 'Scan');
      setState(() {
        user = data;
        return user;
      });
    } 
  }
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 20,
        title:Text('Navigation'),
        brightness: Brightness.light,
        actions: <Widget>[
          Container(
            child: (user != null )
            ? PopupMenuButton(
              icon:Icon(Icons.more_vert, color: Colors.black,size: 30,),
              onSelected: (String value){
                if (value == "exit"){
                  _onExit();
                }
              },
              itemBuilder: (context)=>[
                PopupMenuItem(
                  value: "informacion",
                  child:Text(user == null ? 'Cargando ..' : user['user_name'] )
                
                ),
                PopupMenuItem(
                  value: "exit",
                  child:Text("Log Out")
                
                ),

              ],
              
            )
             : SizedBox(
              width: size.width * 0.05,                                             
            ),
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          _fondoApp(),
          FadeInLeft(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[_titulos(), _botonesRedondeados(context)],
              ),
            ),
          )
        ],
      ),
     
    );
  }

  Widget _fondoApp() {
    final gradiente = Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: FractionalOffset(0.0, 0.3),
              end: FractionalOffset(0.0, 1.0),
              colors: [Colors.blueAccent,Colors.blueAccent[100], Colors.white])),
    );

    final cajaRosada = Transform.rotate(
        angle: -pi / 4.0,
        child: Container(
          height: 360.0,
          width: 360.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(90.0),
            gradient: LinearGradient(colors:[Colors.blueAccent[100], Colors.white]),
          ),
        ));

    return Stack(
      children: <Widget>[
        gradiente,
        Positioned(
          top: -50.0,
          child: cajaRosada,
        )
      ],
    );
  }

  Widget _titulos() {
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 30.0),

            Container(
              width: size.width*0.9,
              child: Column(
                children: <Widget>[

                   ListTile(
                  leading: Icon(Icons.account_balance,color:Colors.black.withOpacity(0.6), size: 40,),
                  title: Text('Welcome',
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.normal,fontSize: 30,color: Color(0xff000a12))),
                   
                  ),
                  SizedBox(height: size.width*0.002),

                  ListTile(
                  leading: user  == null ? Text('') :
                  FaIcon(FontAwesomeIcons.userCircle,color:Colors.black.withOpacity(0.6), size: 40,),
                  title: Text(user  == null ? '' : '${user['user_surname']}, ${user['user_name']}',
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Color(0xff000a12))),
                   
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _botonesRedondeados(context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      child: Table(

        children: [
          TableRow(children: [
            _crearBotonRedondeado(
                Color(0xff0000F1).withOpacity(0.8), Icons.accessibility_new, 'Log In', context, 'loginEn'),
            
            _crearBotonRedondeado(Color(0xff0000F1).withOpacity(0.8), Icons.account_balance, 'museums',
                context, 'museums'),
          ]),

          TableRow(children: [
            _crearBotonRedondeado(
                Color(0xff0000F1).withOpacity(0.8), Icons.star, 'Top Rated', context, 'topRated'),
           
            _crearBotonRedondeado(Color(0xff0000F1).withOpacity(0.8), Icons.library_books, 'Catalogs',
                context, 'articles'),
          ]),
          TableRow(children: [
            _crearBotonRedondeado(
                Color(0xff0000F1).withOpacity(0.8), Icons.developer_board, 'ScanEn', context, 'scanEn'),
            _crearBotonRedondeado(
                Color(0xff0000F1).withOpacity(0.8), Icons.list, 'Categories', context, 'Categories'),
            
          ]),
          

        ],
      ),
    );
  }

  Widget _crearBotonRedondeado(Color color, IconData icono, String texto,
      BuildContext context, String ruta) {
        final size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: GestureDetector(
        onTap: () => Navigator.pushNamed(context, ruta),
        child: Container(
          height: size.height *0.25,
          padding: EdgeInsets.symmetric(horizontal:15),
          decoration: BoxDecoration(
              color: Color(0xff20382A).withOpacity(0.5),
              borderRadius: BorderRadius.circular(20.0)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              CircleAvatar(
                backgroundColor: color,
                radius: 50.5,
                child: Icon(icono,
                color: Color(0xffF8F5F0).withOpacity(0.8), 
                size: 40.0),
              ),
              Text(texto,
                  style: TextStyle(
                    fontSize: 18, 
                    fontWeight: FontWeight.bold,
                    color: Color(0xffFFFFFF).withOpacity(0.8))),
              SizedBox(height: 5.0)
            ],
          ),
        ),
      ),
    );
  }

  _onExit() {
    DialogsEn.confirm(context, title:"CONFIRM",message:"Do you want to log out? ",onCancel:(){
      Navigator.pop(context);
    }, onConfirm: () async{
      Session session = Session();
      final logout = await _authAPI.logout(user["token_type"], user["access_token"]);
      if (logout) {
        session.onClosed();
        Navigator.pushNamedAndRemoveUntil(context,'botones',(_)=>false);
      }else {
        Dialogs.alert(context,title: "Error al Cerrar Sesion", message:'Vuelva intentarlo');
      }

    });
  }
}
