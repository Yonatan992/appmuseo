import 'package:animate_do/animate_do.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lectorqr/src/pages/scope_model/AuthApi/auth_api_service.dart';
import 'package:lectorqr/src/pages/scope_model/apiService.dart';
import 'package:lectorqr/src/pages_en/showScan_en_page.dart';
import 'package:lectorqr/src/utils/dialogs_login_en.dart';
import 'package:lectorqr/src/utils/dialogs_login_es.dart';
import 'package:lectorqr/src/utils/session_login.dart';
import 'package:lectorqr/src/widget/card_swiperSliders_widget.dart';

class ScanEnglish extends StatefulWidget {
  ScanEnglish({Key key}) : super(key: key);

  @override
  _ScanEnglishState createState() => _ScanEnglishState();
}

class _ScanEnglishState extends State<ScanEnglish> {

  String barcode = "";
  Map user;
  final _authAPI = AuthAPI();
  
  final entidadesData = new ApiService();


    @override
    void initState() {
    this.check(); 
    super.initState();
    
    
  }

  final _session = Session();
  check() async {
    Map userr = await _session.getSession();
    if (userr != null && userr['user_verify'] == '1'){
      //Navigator.pushReplacementNamed(context, 'Scan');
      setState(() {
        user = userr;
        return user;
      });
      
    } else if (userr != null && userr['user_verify'] == '0'){
      _confirmarEmail(userr);
    }else {
      Navigator.pushReplacementNamed(context, 'loginEn');

    }
  }


 _scanQR() async{
        try {
      
      String barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);

      if (barcode!= "" && barcode != null){
        Navigator.push(context,
        MaterialPageRoute(
        builder: (context) => ShowScanEnglish(barcode,)));
      }

    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException{
      setState(() => this.barcode = 'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }
  }


 

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(        
        
        brightness: Brightness.light,
        title: Text('QR Scanner Service'),
      
        
        actions: <Widget>[
          PopupMenuButton(
            icon:Icon(Icons.more_vert,),
            onSelected: (String value){
              if (value == "exit"){
                _onExit();
              }
            },
            itemBuilder: (context)=>[
              PopupMenuItem(
                value: "informacion",
                child:Text(user == null ? 'Cargando ..' : user['user_name'] )
              
              ),
              PopupMenuItem(
                value: "exit",
                child:Text("Log Out")
              
              ),

            ],
            
          ),
        ],
      ),
       
      body: Container(
        width: size.width,
        height: size.height,
        padding: EdgeInsets.symmetric(horizontal:20.0),
        margin: EdgeInsets.symmetric(vertical: 20),
        
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: Card(
            elevation: 25,
            shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
            child: ListView(
              children:[
                SizedBox(height: 10,),
                _swiperSliders(),
                SizedBox(height: 10,),
                _welcomeUsers(),
                SizedBox(height: 35,),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: _botonScan(),
                ),
                

              ]
            ),
          ),
        ),
      ),

      
    );
  }

  _onExit() {
    DialogsEn.confirm(context, title:"CONFIRM",message:"Do you want to log out? ",onCancel:(){
      Navigator.pop(context);
    }, onConfirm: () async{
      Navigator.pop(context);
      Session session = Session();
      await session.onClosed();
      Navigator.pushNamedAndRemoveUntil(context,'buttons',(_)=>false);

    });
  }

  Widget _swiperSliders() {
    return SwiperSliders();
  }

  Widget _welcomeUsers() {
    return Container(
      child: Column(
        children: [
          
             Text('Welcome',
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.normal,fontSize: 20,color: Colors.black)),
            
             Text(user  == null ? 'Cargando ..' : '${user['user_surname']}, ${user['user_name']}',
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.black)),

        ]
      ),

    );
  }

  Widget _botonScan() {
      return FadeInLeftBig(
              child: RaisedButton(
              color: Color(0xff2962ff),
              textColor:Colors.black,
              shape: RoundedRectangleBorder(
                    borderRadius:BorderRadius.circular(15)
                    ),
              onPressed: _scanQR,
              child: Row (
                  mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FaIcon(FontAwesomeIcons.qrcode, size:50, color: Colors.white ),
                      SizedBox(width:10,),
                      Text('Press Scanner QR',
                        style:GoogleFonts.bevan(textStyle: TextStyle(color: Colors.white, letterSpacing: .5,fontSize: 15)))

                    ],)
                    
    ),
      );

  } 

  _confirmarEmail(Map userr)async {
      await showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title:  Text('Confirm your email'),
              content: Text('The system will close the session'),
              actions: [
                new FlatButton(
                  child: new Text("to accept"),
                  onPressed: () => Navigator.of(context).pop()
                ),
              ],
            ),
      );
         final logout = await _authAPI.logout(userr["token_type"], userr["access_token"]);
      if (logout) {
        Session session = Session();
        await _authAPI.getResendEmail(userr['user_id']);
        session.onClosed();
        
      }else {
        Dialogs.alert(context,title: "Logout Error", message:'Try again');
      }

      Navigator.pushNamedAndRemoveUntil(context,'bottons',(_)=>false);
        
  }
  
}
