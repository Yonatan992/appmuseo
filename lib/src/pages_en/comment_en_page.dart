import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lectorqr/src/models/articulos_model.dart';
import 'package:lectorqr/src/pages/scope_model/apiService.dart';
import 'package:lectorqr/src/pages_en/showScan_en_page.dart';
import 'package:lectorqr/src/utils/session_login.dart';
import 'package:rating_bar/rating_bar.dart';

class CommentEnglish extends StatefulWidget {

  final Articulo articulo;
  CommentEnglish({this.articulo});

  @override
  _CommentEnglishState createState() => _CommentEnglishState();
}

class _CommentEnglishState extends State<CommentEnglish> {


  final _apiService = ApiService();
  Map user;


   final _formKey = GlobalKey<FormState>();
   String _comentario ='';
  double _ratingStar = 0.0;
  var _isFetching = false;


   final _session = Session();
  check() async {
    final data = await _session.getSession();
    if (data != null){
      //Navigator.pushReplacementNamed(context, 'Scan');
      setState(() {
        user = data;
        return user;
      });
      
    } else {
      Navigator.pushReplacementNamed(context, 'loginEn');
    }
  }


    @override
  void initState() {
    this.check(); 
    super.initState();

  }


  @override
  void dispose() {
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    

    return Container(
       child: Scaffold(
         appBar: AppBar(       
        brightness: Brightness.light,
        title: Text('Comment',
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontWeight: FontWeight.bold)),
        backgroundColor: Colors.blueAccent,
      ),

      body: ListView(
        padding: EdgeInsets.symmetric(horizontal:10.0,vertical:20),
        children: <Widget> [
          _headerComentario(),
          _crearComentario(),

        ]
      )
    ),

    
    );
  }

  Widget _crearComentario() {

    return Card(
      elevation: 30.5,
      shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0)),
      child: Column(
          children: <Widget>[
            Container(
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[

                    ListTile(
                      leading: FaIcon(FontAwesomeIcons.star,color:Colors.black.withOpacity(0.6), size: 20,),
                      title: Text('Rating :   $_ratingStar',
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontWeight: FontWeight.normal,fontSize: 20,color: Color(0xff000a12))),
                      subtitle: RatingBar(
                      
                        onRatingChanged: (rating) => setState(() =>  _ratingStar = rating,), 
                        filledIcon: Icons.star,
                        filledColor: Colors.yellow,
                        emptyIcon: Icons.star_border,
                    ),
                    ),
                    SizedBox(height: 20),

                    Text('Share details about your experience with the Scanned Article',style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10.0,color:Color(0xff000a12))),
                    
                    Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                            decoration:  InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                            counter: Text ('Letters ${_comentario.length}'),
                            labelText: 'Comment',
                            helperText: 'Brief comment up to 300 characters',
                            icon: Icon(Icons.comment),
                                  ),
                            maxLength: 300,
                            textInputAction: TextInputAction.newline,
                            keyboardType: TextInputType.multiline,
                            maxLines: 5,
                            onChanged: (value){
                                    setState(() {
                                      _comentario = value;
                                    });
                            },
                            
                        ),
                              ),

               


                                ],

                            )),

            ),
            SizedBox(height: 15),
                
           

            
           Row(
                  children: <Widget>[
                      SizedBox(width: 130),
                      ClipRRect(
                          borderRadius: BorderRadius.circular(10.0),
                          child: RaisedButton(
                            color: Color(0xFFbdbdbd),
                            onPressed: () {
                               Navigator.pop(context);
                            },
                            textColor: Colors.white,

                            padding: const EdgeInsets.all(1.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: Container(
                                decoration: const BoxDecoration(
                                  color: Color(0xFF448aff),
                                ),
                                padding: const EdgeInsets.all(10.0),
                                child: const Text(
                                  'Cancel',
                                  style: TextStyle(fontSize: 25)
                                ),
                              ),
                            ),
                          ),
                        ),

                      SizedBox(width: 20),
                      ClipRRect(
                           borderRadius: BorderRadius.circular(10.0),
                           child: RaisedButton(
                             splashColor: Colors.red[200],
                             animationDuration: Duration(seconds: 2),
                             color: Color(0xFFbdbdbd),
                            onPressed: _ratingStar == 0.0 ? null : () => _enviarComentario(),
                            textColor: Colors.white,
                            padding: const EdgeInsets.all(1.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: Container(
                                decoration: const BoxDecoration(
                                  color: Color(0xFF448aff),
                                ),
                                padding: const EdgeInsets.all(10.0),
                                child: const Text(
                                  'To post',
                                  style: TextStyle(fontSize: 25)
                                ),
                              ),
                            ),
                        ),
                      ),
                  ],


                    ),
            
            SizedBox(height: 2),
            Center(
                child: Container(
                padding: EdgeInsets.all(20.0),
                child: _isFetching ? LinearProgressIndicator() : Text("The review will be public"),
                ),
            ),

            ],
            
          ),
          
        );

  }

  void _enviarComentario() async{

    if (_isFetching)return;

    final isValid = _formKey.currentState.validate();
          if (isValid){
            setState(() {
              _isFetching =true;
            });
    final isResponse = await _apiService.storeComentario( context,
                                          articulo: widget.articulo.identificador,
                                          usuario: user['user_id'],
                                          puntuacion:_ratingStar.round(),
                                          comentario: _comentario,
                                          );
            setState(() {
              _isFetching =false;
            });

              if (isResponse){
                
                _respuesta();
        
              }

          }

  }

  _headerComentario() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(5.0),
      child: Card(
        color: Color(0xfffafafa).withOpacity(0.9),
        child: Column(
          children:<Widget>[

            ListTile(
              leading: FaIcon(FontAwesomeIcons.comments,color:Colors.black.withOpacity(0.6), size: 20,),
              title: Text(user  == null ? 'Cargando ..' : '${widget.articulo.tituloEn}',
              textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Color(0xff000a12))),
              subtitle: Text(user  == null ? 'Cargando ..' : '${widget.articulo.denominacionEnt}',
                      style: TextStyle(fontWeight: FontWeight.normal,fontSize: 15,color: Color(0xff000a12))),
               
            ),
            ListTile(
              leading: FaIcon(FontAwesomeIcons.userCircle,color:Colors.black.withOpacity(0.6), size: 20,),
              title: Text(user  == null ? 'Cargando ..' : '${user['user_surname']}, ${user['user_name']}',
              textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15,color: Color(0xff000a12))),
               
            ),

          ]
        ),
      ),
    );
  }

   _respuesta() async {
      await showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Text('Thank you very much for your evaluation.'),
              actions: [
                new FlatButton(
                  child: new Text("see review"),
                  onPressed: () => Navigator.pop(context),
                ),
              ],
            ),
      );
       Navigator.pushReplacement(context,
        MaterialPageRoute(
        builder: (context) => ShowScanEnglish(widget.articulo.slug)));
    }
  
}