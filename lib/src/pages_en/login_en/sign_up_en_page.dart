import 'package:animate_do/animate_do.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lectorqr/src/pages/scope_model/AuthApi/auth_api_service.dart';
import 'package:lectorqr/src/widget/circle_login_widget.dart';
import 'package:lectorqr/src/widget/input_login_widget.dart';

class SignupEnglish extends StatefulWidget {
  SignupEnglish({Key key}) : super(key: key);

  @override
  _SignupEnglishState createState() => _SignupEnglishState();
}

class _SignupEnglishState extends State<SignupEnglish> {

  final _formKey = GlobalKey<FormState>();
  final _authAPI = AuthAPI();

  var _profile = 3,_entity ='',_firstname ='', _lastname ='', _email ='', _password ='',_lang ='en';
  var _isFetching = false;

  bool _obscureText = true;

   void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  void initState() {
    
    super.initState();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

  }

// metod que se llama cuando doy click en el boton Crear Cuenta
  _submit() async{
  
   if (_isFetching)return;

   final isValid = _formKey.currentState.validate();
   if (isValid){
     setState(() {
        _isFetching =true;
      });

     final isResponse = await _authAPI.register( context,
                                  profile: _profile,
                                  entity: _entity,
                                  lang: _lang,
                                  firstname: _firstname, 
                                  lastname: _lastname,
                                  email: _email,
                                  password: _password);

      setState(() {
        _isFetching =false;
      });

      if (isResponse){
        _respuestaRegistro();
       
      }

   }
  }
  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: GestureDetector(
        onTap: (){
          // con esto se minimiza el teclado cuando doy click afuera del text
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
            width: size.width,
            height: size.height,
            child: Card(
              elevation: 15,
              child: Stack(
               children: <Widget>[

                Positioned(
                right: -size.width*0.25,
                top: -size.width*0.40,
                child: CircleLogin(
                        radius: size.width*0.45,
                        colors: [
                         Color(0xffc7c7c7).withOpacity(0.3),
                          Color(0xffc7c7c7).withOpacity(0.4),
                        ]
                      ),
              ),


               Positioned(
                  right: size.width*0.05,
                  top: size.width*0.55,
                  child: CircleLogin(
                          radius: size.width*0.07,
                          colors: [
                            
                            Colors.blueAccent, 
                            Color(0xff2979ff).withOpacity(0.8),
                           
                            
                          ]
                        ),
                ),


                Positioned(
                  right: size.width*0.80,
                  top: size.width*0.45,
                  child: CircleLogin(
                          radius: size.width*0.08,
                          colors: [
                            
                            Color(0xffc7c7c7).withOpacity(0.3),
                            Color(0xffc7c7c7).withOpacity(0.4),
                           
                            
                          ]
                        ),
                ),

              Positioned(
                left: -size.width*0.30,
                top: -size.width*0.48,
                child: CircleLogin(
                        radius: size.width*0.40,
                        colors: [
                          Colors.blueAccent, 
                          Color(0xff2979ff).withOpacity(0.8),
                        ]
                      ),
              ),

              SafeArea(
                      child: SingleChildScrollView(
                        child: Container(
                           width: size.width,
                          height: size.height,
                          child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[

                                  Column(
                                    children: <Widget>[
                                      SizedBox(height: size.height * 0.030),
                                       Container(
                                          width: size.width * 0.20,
                                          height: size.height * 0.10,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(10),
                                            image: new DecorationImage(
                                              image: new ExactAssetImage('assets/registro.png'),
                                              fit: BoxFit.cover,
                                        ),
                                            
                                        ),
                                  ),

                                  SizedBox(height: size.height * 0.010),

                                  Text('Create Your Account',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 20 , fontWeight: FontWeight.normal),
                                  
                                  ),

                              ],
                            ),

                  BounceInLeft(
                    child: Container(
                          
                          width: size.width * 0.9,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              color: Color(0xff8D8D8D).withOpacity(0.2),
                              
                          ),
                          child: Card(
                            elevation: 15,
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: size.height * 0.010),
                                    Container(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Form(
                                                key: _formKey,
                                                child:Column(
                                                  children: <Widget>[
                                                    InputText(label: 'Enter your Name',
                                                              fontSize: 20,
                                                              validator: (String text){
                                                                if (text.isNotEmpty){
                                                                  _firstname = text;
                                                                  return null;
                                                                }
                                                                return "Enter your Name";
                                                              }),
                                                        SizedBox(height: size.height * 0.010),
                                                      InputText(label: 'Enter your Surname',
                                                              fontSize: 20,
                                                              validator: (String text){
                                                                if (text.isNotEmpty){
                                                                  _lastname = text;
                                                                  return null;
                                                                }
                                                                return "Enter your Surname";
                                                              }),
                                                    SizedBox(height: size.height * 0.010),
                                                    InputText(label: 'Email',
                                                              fontSize: 20,
                                                              inputType: TextInputType.emailAddress,
                                                              validator: (String text){
                                                                if (text.contains("@")){
                                                                  _email = text;
                                                                  return null;
                                                                }
                                                                return "Invalid Email";
                                                              }),
                                                    SizedBox(height: size.height * 0.010),
                                                    InputText(label: 'Password',
                                                              fontSize: 20,
                                                              isSecure: _obscureText,
                                                              validator: (String text){
                                                                if (text.isEmpty) { 
                                                                  return "Please enter password";
                                                                } else if(text.length < 7){
                                                                  return "Must be more than 6 charater";

                                                                }

                                                                _password = text;
                                                                return null;
                                                              }
                                                              
                                                              ),
                                                    FlatButton(
                                                       onPressed: _toggle,
                                                        child: new Text(_obscureText ? "Show" : "Hide"))
                                                  ],
                                                )
                                              ),
                                      ),
                                    ),

                              Container(
                                child: CupertinoButton(
                                          
                                          color: Color(0xff2962ff),
                                          borderRadius: BorderRadius.circular(10),
                                          onPressed: ()=> _submit(),
                                            child: Text('Register now',
                                                          style: TextStyle(fontSize: 20),),

                                                ),
                              ),
                                    
                                      
                              
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Already have an account?',
                                              style: TextStyle(fontSize: 15,color: Colors.black54),),
                                          CupertinoButton(
                                            onPressed: ()=>Navigator.pop(context),
                                            child: Text('Login',
                                              style: TextStyle(fontSize: 15,color: Color(0xff69409E),fontWeight: FontWeight.bold),),
                                          )
                                  

                                  ],
                              ),
                              SizedBox(height: size.height * 0.020),
                                
                            ],
                              ),
                          ),
                    ),
                  ),


                    ],
                ),
                        ),
                      ),
              ),

                Positioned(
                  left: 15,
                  top: 15,
                  child: SafeArea(
                          child: CupertinoButton(
                          padding: EdgeInsets.all(15),
                          borderRadius: BorderRadius.circular(40),
                          color: Colors.black38,
                          onPressed: ()=>Navigator.pop(context),
                          child: Icon(Icons.arrow_back, 
                                      color:Colors.white),
                        ),
                  )
                ),


                // SI ESTOY REALIZACION UNA PETICION A MI SERVIDOR MOSTRAME EL ACTIVITYINDICATOR
                // CASO CONTRARIO MUESTRA EL CONTAINER VACIO
               _isFetching ? Positioned.fill(child: Container(
                  color: Colors.black45,
                  child: Center(
                    child: CupertinoActivityIndicator(radius: 15),
                  ),
                )) : Container()

          

          ],

        ),
            ),


      ),

      )
    );
  }

 void _respuestaRegistro() async {
      await showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text('You have successfully registered'),
              content: Text('You will receive an email to verify your account', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15,color: Color(0xff000a12))),
              actions: [
                new FlatButton(
                  child: new Text("To accept"),
                  onPressed: () => Navigator.of(context).pop()
                ),
              ],
            ),
      );
       Navigator.pushReplacementNamed(context,'buttons');
    }
 
}