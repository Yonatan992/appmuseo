import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lectorqr/src/pages/scope_model/AuthApi/auth_api_service.dart';
import 'package:lectorqr/src/widget/input_login_widget.dart';

class ResetPassEnglish extends StatefulWidget {
  @override
  _ResetPassEnglishState createState() => _ResetPassEnglishState();
}

class _ResetPassEnglishState extends State<ResetPassEnglish> {
   final _formKey = GlobalKey<FormState>();

  final _authAPI = AuthAPI();
  var _isFetching = false;

  var _email = '';

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(        
        brightness: Brightness.light,
        title: Text('Forgot password',)
      ),

      body: Center(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(30.0),
          child: Card(
            color: Color(0xffeeeeee),
            child: Container(
              width: size.width * 0.90,
              height: size.height * 0.70,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListView(
                  
                  children: <Widget> [
                    SizedBox(height:size.height * 0.05),
                    _headerForm(),
                    _formReset(),

                  ]
                ),
              ),
            ),
          ),
        ),
      )


    );
  }

  _headerForm() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10.0),
      child: Card(
        elevation: 15,
        color: Color(0xfffafafa).withOpacity(0.9),
        child: Column(
          children:<Widget>[
            ListTile(
              leading: FaIcon(FontAwesomeIcons.unlock,color:Colors.black.withOpacity(0.6), size: 20,),
              title: Text('Restore password',
              textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Color(0xff000a12))),
               
            ),

          ]
        ),
      ),
    );
    
  }

  Widget _formReset() {
    final size = MediaQuery.of(context).size;
    return Stack( 
      children:<Widget>[
        Container(
        child: Column(
            children: <Widget>[
              Container(
                child: Form(
                  key: _formKey,
                      child:Column(
                          children: <Widget>[
                            SizedBox(height: size.height * 0.040),
                              InputText(label: 'Enter email',
                                fontSize: 20,
                                inputType: TextInputType.emailAddress,
                                validator: (String text){
                                  if (text.contains("@")){
                                    _email = text;
                                  return null;
                                  }
                                  return "Invalid Email";
                                }),
                            SizedBox(height: size.height * 0.050),
                                                  
                                                       
                                                        
                          ],
                      )
                ),

              ),
            
              ClipRRect(
                borderRadius: BorderRadius.circular(15.0),
                  child: RaisedButton(
                      splashColor: Colors.red[200],
                      animationDuration: Duration(seconds: 2),
                      color: Color(0xff2962ff),
                      onPressed: () => _resent(context),
                      textColor: Colors.white,
                      padding: const EdgeInsets.only(left:60,right:60,top:20,bottom:20),
                      child:Text('Send email',
                        style: TextStyle(fontSize: 15)
                        ),
                  ),
              ),

              Center(
                    child: Container(
                    padding: EdgeInsets.all(30.0),
                    child: _isFetching ? LinearProgressIndicator() : Text(" "),
                    ),
                ),

          ],
              
            ),
      ),

      

    ]
  );
}

  _resent(BuildContext context) async {
    if (_isFetching)return;

    final isValid = _formKey.currentState.validate();
        if (isValid){
          setState(() {
              _isFetching =true;
            });
        final isResponse = await _authAPI.resetPass(context, email: _email);
                  
            setState(() {
              _isFetching =false;
            });

          if (isResponse){
                  _respuesta();
           
          }else {
            _respuestaError();
          }
            
      }
            
            
            
            
              }
            
   _respuesta() async {
                  await showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                          content: Text('Password reminder sent! , check your mail'),
                          actions: [
                            new FlatButton(
                              child: new Text("return"),
                              onPressed: () => Navigator.of(context).pop()
                            ),
                          ],
                        ),
                  );
                   Navigator.of(context).pop();
              }
            
  _respuestaError() async{
    await showDialog(
          context: context,
          builder: (context) => AlertDialog(
                    title: Text('We register an error'),
                    content: Text('Verify your email'),
                      actions: [
                        new FlatButton(
                          child: new Text("Try again"),
                          onPressed: () => Navigator.of(context).pop()
                            ),
                        ],
                  ),
          );
  }
}



