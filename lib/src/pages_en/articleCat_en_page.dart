import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lectorqr/src/models/articulos_model.dart';
//import 'package:lectorqr/src/pages/scope_model/var_global.dart';
import 'package:lectorqr/src/pages/scope_model/apiService.dart';

class ArticleCatPage extends StatelessWidget {
  final int _category;
  //final Ruta _ruta = Ruta();
  ArticleCatPage(this._category);
  final _apiService = ApiService();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('List of catalogs by category'),
      ),
      
      body:  FutureBuilder(
      future:  _apiService.getArticulosForCategory(_category),
      builder: (BuildContext context,AsyncSnapshot <List<Articulo>> articulos) {
        if (articulos.connectionState == ConnectionState.done) {
          return Center(
            child: (articulos.data.length == 0)
                  ? Container(
                    width: size.width,
                    height: size.height * 0.30,
                    child: Card(
                      elevation: 15,
                    child: Column(
                      children: <Widget>[
                        Text('There are no articles associated to the category',
                            style: TextStyle(
                            fontSize: 15.0)),
                        SizedBox(height:size.height * 0.030),
                        FaIcon(FontAwesomeIcons.exclamationCircle,color:Colors.black.withOpacity(0.6), size: 80,),
                      ],
                    )),
                  )
                  :ListView.builder(
                    itemCount: articulos.data.length,
                    itemBuilder: (context, int i) {
                            Articulo articulo = articulos.data[i];
                            return _listaArticulos(context,articulo);
                          }

                  ),
          );
          
          
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },

    )

  );
  }

  Widget _listaArticulos(context,Articulo articulo) {
    final size = MediaQuery.of(context).size;
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          child: Column(
            children: <Widget>[
              SizedBox(height: size.height * 0.05,),
              Card(
                  elevation: 20.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color:  Color(0xff2979ff).withOpacity(0.6),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 30.0),
                      Text(articulo.denominacionEnt == null ? 'Cargando ..' : articulo.denominacionEnt,
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style:GoogleFonts.montserrat(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))),
                      Divider(
                          color: Colors.white,
                          height: size.height * 0.020,),
                      Text(articulo.tituloEn == null ? 'Cargando ..' : articulo.tituloEn,
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                         style:GoogleFonts.bevan(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))),
                      SizedBox(height: 30.0),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(5.0),
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal:20),
                          width: 350,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              color:  Color(0xfffafafa),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: Color(0xfffafafa),
                                    blurRadius: 8.0,
                                    spreadRadius: 2.0,
                                    offset: Offset(2.0, -10.0))
                              ]),
                            child: Column(
                              children: <Widget>[
                                ClipRRect(
                                    borderRadius: BorderRadius.circular(15.0),
                                    child: (articulo.imagen != null) 
                                            ?FadeInImage(
                                              image: NetworkImage(articulo.imagen),
                                              width: size.width,
                                              placeholder: AssetImage('assets/giphy.gif'),
                                              fadeInDuration: Duration(milliseconds: 700),
                                              height: 250.0,
                                              fit: BoxFit.cover,
                                        )
                                        : Image(image:AssetImage('assets/no-image.png'),)
                                  ),
                                SizedBox(height: 10.0),

                                Text('Categoria: ' + articulo.denominacionCatEn == null ? 'Cargando ..' : articulo.denominacionCatEn,
                                    textAlign: TextAlign.right,
                                    style:GoogleFonts.bevan(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 20))),
                                SizedBox(height: 20.0),
                                Text(articulo.descripcionEn == null ? 'Cargando ..' : articulo.descripcionEn,
                                    textAlign: TextAlign.justify,
                                     style:GoogleFonts.lato(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))),
                                SizedBox(height: 40.0),
                              ],
                            ),
                          
                        ),
                      ),
                      SizedBox(height: 40.0),
                    ],
                  ),
                )
            ],
          ),

        ),
      ),
    );
  }
}
