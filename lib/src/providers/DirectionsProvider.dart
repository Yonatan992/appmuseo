import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_maps_webservice/directions.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' as maps;
class DirectionProvider extends ChangeNotifier{
  GoogleMapsDirections directionsApi = GoogleMapsDirections(apiKey:'AIzaSyDh-9-BZZeihMvOdlEsODXmvhgwfDlEhlw');

  Set<maps.Polyline> _route = Set();

  Set<maps.Polyline> get currentRoute => _route;

  findDirections(maps.LatLng from, maps.LatLng to) async {
    var origen = Location(from.latitude,from.longitude);
     var destino = Location(to.latitude,to.longitude);

    var resultado = await directionsApi.directionsWithLocation(
      origen, 
      destino);

    Set<maps.Polyline> newRoute = Set();
  if (resultado.isOkay){
    var route = resultado.routes[0];
    var leg = route.legs[0];

    List<maps.LatLng> points = [];
    leg.steps.forEach((step){
      points.add(maps.LatLng(step.startLocation.lat, step.startLocation.lng));
      points.add(maps.LatLng(step.endLocation.lat, step.endLocation.lng));
    });

    var linea = maps.Polyline(
      points: points,
      polylineId: maps.PolylineId('Ruta para llegar a destino'),
      color: Colors.red,
      width: 5,
    );

    newRoute.add(linea);
    _route = newRoute;
    notifyListeners();
  }

  }



}