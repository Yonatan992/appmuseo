import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
//import 'package:lectorqr/src/pages/scope_model/var_global.dart';
import 'package:lectorqr/src/pages/scope_model/apiService.dart';
import 'package:lectorqr/src/pages_en/entidadArt_en_page.dart';

class DataSearchEntities extends SearchDelegate {
  String seleccion = ' ';
  //final Ruta _ruta = Ruta();

  

  @override
  List<Widget> buildActions(BuildContext context) {
    // Las acciones de nuestro AppBar (por ejemplo un iconito para limpiar el texto o cancelar la busqueda)
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
          showSuggestions(context);
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // es algo que aparece al inicio (en nuestro caso es el icono a la izquierda del AppBar)

    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // Crea los Resultados que vamos a mostrar
    return Center(
      child: Container(
        height: 100.0,
        width: 100.0,
        color: Colors.blueAccent,
        child: Text(seleccion),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // sugerencias que les aparecen cuando las personas escribe
    final size = MediaQuery.of(context).size;
    if (query.isEmpty) {
      return SingleChildScrollView(
              child: SafeArea(
                child: Center(
                  child: Container(
            height: MediaQuery.of(context).size.height * 0.80,
            width: MediaQuery.of(context).size.width * 0.90,
            decoration: BoxDecoration(
              color: Colors.blueAccent.withOpacity(0.6),
              borderRadius: BorderRadius.circular(25),
            ),
            child: Column(
              children: <Widget>[
                  
                  SizedBox(height:size.height * 0.25),
                  Text(
                      'Search museums and look at their articles.',
                      textAlign: TextAlign.center,
                      style:GoogleFonts.playfairDisplay(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 20)),
                    ),

                  SizedBox(height:size.height * 0.04),
                  
                  Container(
                    width: size.width * 0.50,
                    height: size.height * 0.30,
                    child: Image(
                      image: AssetImage('assets/searchMuseum.png'),
                      color: Colors.black54,
                      fit: BoxFit.cover,
                    ),
                  ),
              ],
            ),
          ),
                ),
        ),
      );
    }

    return FutureBuilder(
      future: ApiService.buscarEntidad(query),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        
        if (snapshot.hasData == false){
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(child: CircularProgressIndicator()),
              ],
                );

        } else{
                return 
                 Center(
                    child: (data.length > 0)
                   ? ListView.builder(
                      itemCount: data == null ? 0 : data.length,
                      itemBuilder: (BuildContext context, int i) {
                        return ClipRRect(
                          borderRadius:BorderRadius.circular(20.0),
                          child: Card(
                            elevation: 15,
                          child:ListTile(
                            leading: FadeInImage(
                              image: NetworkImage(data[i]["imagen"]),
                              placeholder: AssetImage('assets/no-image.jpg'),
                              fadeInDuration: Duration(milliseconds: 700),
                              width: 50.0,
                              fit: BoxFit.contain,
                            ),
                            title: Text('Museum: '+data[i]["denominacion"]),
                            subtitle: Text('Location: '+data[i]["ubicacion"]),
                            onTap: () {
                              close(context, null);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          EntidadesArtEnPage(data[i]['identificador'])));
                            },
                          ),
                        ),
                      );
                    })
                   : Column(
                    children: <Widget>[
                      SizedBox(height:10),
                      Text("No results found for " +" ' $query ' ",
                        
                      ),
                    ],
                ),
              );
        }
      },
    );
  }
}
