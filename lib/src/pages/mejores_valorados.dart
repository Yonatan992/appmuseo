import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lectorqr/src/pages/scope_model/apiService.dart';
import 'package:lectorqr/src/pages/scope_model/var_global.dart';
import 'package:rating_bar/rating_bar.dart';

class MejoresValPage extends StatefulWidget {

  @override
  _MejoresValPageState createState() => _MejoresValPageState();
}

class _MejoresValPageState extends State<MejoresValPage> {

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        body: Stack(
          children:<Widget>[
             _MainScroll(),

             Positioned(
               bottom: -10,
               right: 0,
               child: _BotonNavegar())
             
          ],
        )
      );
  }
}

class _BotonNavegar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;
    return ButtonTheme(
      minWidth: size.width * 0.5,
      height: 80,
      child: RaisedButton(
        onPressed: (){
          Navigator.pop(context);
        },
        color: Colors.blue,
        shape: RoundedRectangleBorder(
          borderRadius:BorderRadius.only(topLeft:Radius.circular(60))
        ),
        child: Text('Regresar',
                style: TextStyle(
                  color:Colors.white,
                  fontSize:18,
                  fontWeight:FontWeight.bold,
                  letterSpacing: 4
                ),),

      
      ),


    );
  }
}

class _Titulo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
      child: Column(
        children: <Widget>[
          SizedBox(height:20),
          Container(
            margin: EdgeInsets.symmetric(horizontal:30, vertical:20),
            child: Text('Artículos mejores valorados', style:GoogleFonts.montserrat(textStyle: TextStyle(color: Colors.white, letterSpacing: .5,fontSize: 20)))
          ),

          Stack(
            children: <Widget>[
              SizedBox(height:10),
              Positioned(
                bottom: 55,
                child: Container(
                  width: 120,
                  height: 10,
                  color: Color(0xfff5f5f5)

                ),
              ),

              Container(
                 child: Text('Por Museos', style:GoogleFonts.montserrat(textStyle: TextStyle(color: Colors.white, letterSpacing: .5,fontSize: 20)))
              ),

            ],
          )

        ],
      ),
    );
  }
}


class _MainScroll extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final _apiService = ApiService();

  return FutureBuilder (
        future: _apiService.mejoresValorados(),
        builder: (BuildContext context,AsyncSnapshot snapshot) {
          Widget newsListSliver;
          if (snapshot.connectionState == ConnectionState.done) {
           newsListSliver = SliverList(delegate: SliverChildBuilderDelegate(

             (context, int i){
                return (valorados == null)
                      ? Center(child: CircularProgressIndicator())
                    :Container(
                      
                      child:Card(
                        
                    
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0)),
                    color: Color(0xfff5f5f5).withOpacity(0.8),
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 30.0),
                        Container(
                          width: 350,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color:  Color(0xfff5f5f5),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(30.0),
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: 30.0),
                                ClipRRect(
                                   borderRadius: BorderRadius.only(topLeft:Radius.circular(20),bottomRight:Radius.circular(20)),
                                  child: (valorados[i]["imagen_articulo"] != null) 
                                        ? FadeInImage(
                                    image: NetworkImage(Urls.URL_IMAGEN+valorados[i]["imagen_articulo"]),
                                    width: 320,
                                    placeholder: AssetImage('assets/giphy.gif'),
                                    fadeInDuration: Duration(milliseconds: 700),
                                    height: 210.0,
                                    fit: BoxFit.cover,
                                  )
                                  : Image(image:AssetImage('assets/no-image.png'),)
                                ),

                                Row(
                                  children: <Widget>[
                                    SizedBox(width: 15.0),
                                    Container(
                                      width: 300,
                                      child: Text(valorados[i]["nombre_articulo"],
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        style:GoogleFonts.bevan(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))),
                                    ),

                                  ],
                                ),

                                Row(
                                  children: <Widget>[
                                      SizedBox(width: 10.0),
                                      RatingBar.readOnly(
                                        initialRating:double.parse(valorados[i]['promedio_articulo']),
                                        isHalfAllowed: true,
                                        filledColor: Colors.yellow,
                                        halfFilledIcon: Icons.star_half,
                                        filledIcon: Icons.star,
                                        emptyIcon: Icons.star_border,
                                      ),
                                    
                                    SizedBox(width: 10.0),
                                    Text(double.parse(valorados[i]["promedio_articulo"]).toStringAsFixed(1),
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                          fontSize: 20.0)),
                  
                                  ],

                                ),
                            
                                ListTile(
                                  leading: Icon(Icons.account_balance,color:Colors.black.withOpacity(0.6), size: 40,),
                                  title:Text(valorados[i]["nombre_museo"],
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      style:GoogleFonts.alegreyaSans(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))) ,
                                ),
                                SizedBox(height: 30.0),

                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 40.0),
                        Divider()
                      ],
                    ),
                  ),
                
            
          );
        },
      childCount: valorados == null ? 0 : valorados.length,
        
      )

      
      );
            
            
          } else {
            newsListSliver = SliverToBoxAdapter(child: Center(
            child: CircularProgressIndicator(),
          ));
          }



    return CustomScrollView(
     slivers: <Widget>[

       SliverPersistentHeader(
         floating: true,
         delegate: _SliverCustomHeaderDelegate(
           minheight: 200,
           maxheight: 200,
           child: Container(
             color:Colors.white,
             child: _Titulo()
           )
         )),
      
       newsListSliver

     ],
   );
         
        },
      );


   
  }
}

class _SliverCustomHeaderDelegate extends SliverPersistentHeaderDelegate{

  final double minheight;
  final double maxheight;
  final Widget child;
  _SliverCustomHeaderDelegate({ 
    @required this.minheight, 
    @required this.maxheight, 
    @required this.child });

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    
    return SizedBox.expand(child: child);
  }

  @override
  double get maxExtent => maxheight;

  @override
  double get minExtent => minheight;

  @override
  bool shouldRebuild(_SliverCustomHeaderDelegate oldDelegate) {
   
   return maxheight != oldDelegate.maxheight ||
          minheight != oldDelegate.minheight ||
          child     != oldDelegate.child;
  }



}
