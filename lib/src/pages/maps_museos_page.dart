import 'dart:math';
import 'dart:ui' as ui;
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:lectorqr/src/models/entidad_model.dart';
import 'package:geolocator/geolocator.dart';
import 'package:lectorqr/src/providers/DirectionsProvider.dart';
import 'package:provider/provider.dart';


class MapsMuseos extends StatefulWidget {
  final Entidad entidad;
  MapsMuseos({this.entidad});

  @override
  _MapsMuseosState createState() => _MapsMuseosState();
}

class _MapsMuseosState extends State<MapsMuseos> {
  
 bool mapToogle =false;
  var posicionActual;
  BitmapDescriptor pinLocationMuseo;
  BitmapDescriptor pinLocation;


  @override
  void initState() {
    super.initState();
    Geolocator().getCurrentPosition().then((currloc){
      setState(() {
        posicionActual = currloc;
        mapToogle = true;
      });

    });

    BitmapDescriptor.fromAssetImage(
         ImageConfiguration(devicePixelRatio: 0.5),
         'assets/museum.png').then((onValue) {
            pinLocationMuseo = onValue;
    });

     BitmapDescriptor.fromAssetImage(
         ImageConfiguration(devicePixelRatio: 0.5),
         'assets/signs.png').then((onValue) {
            pinLocation = onValue;
    });


  }


  
  
  //final LatLng fromPoint = LatLng (-27.519423, -58.793746);
  //final LatLng toPoint = LatLng (-27.465275, -58.848952);
  
  GoogleMapController _mapController;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title:Text('${widget.entidad.denominacion}')
      ),
      body: Consumer<DirectionProvider>(
        builder:(BuildContext context, DirectionProvider api, Widget child){
           return Column(
             children: <Widget>[
               Center(
                 child: Card(
                   elevation: 15,
                   child: ClipRRect(
                     borderRadius: BorderRadius.circular(10.0),
                     child: Container(
                       height: MediaQuery.of(context).size.height * 0.80,
                       width: MediaQuery.of(context).size.width * 0.90,
                       child: mapToogle ? GoogleMap(
                       initialCameraPosition: CameraPosition(
                         target: LatLng (posicionActual.latitude,posicionActual.longitude),
                         zoom: 16),
                       markers: _crearMarkers(),
                       polylines: api.currentRoute,
                       onMapCreated: _onMapCreated,
                       //ubicacion actual
                       myLocationEnabled: true,
                       myLocationButtonEnabled: true,


                      ) 
                      : Center(child:
                        Text('Cargando ... Espere por favor',
                          style: TextStyle(fontSize:20.0)))

                     ),
                   ),
                 ),
               ),
             ],
           );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.zoom_out_map),
        onPressed: _centerView,
        
        ),
    );
  }

  Set<Marker> _crearMarkers(){
    
    final LatLng fromPoint = LatLng (posicionActual.latitude,posicionActual.longitude);
    final LatLng toPoint = LatLng (widget.entidad.latitud.toDouble(),widget.entidad.longitud.toDouble());
    
    var tmp = Set<Marker>();
    tmp.add(Marker(
      markerId: MarkerId('fromPoint'),
      position: fromPoint,
      infoWindow: InfoWindow(title:'Ubicación actual'),
      icon: pinLocation,
      ),
    );
    tmp.add(Marker(
      markerId: MarkerId('toPoint'),
      position: toPoint,
      infoWindow: InfoWindow(title:'${widget.entidad.denominacion}'),
      icon: pinLocationMuseo,
    ));

    return tmp;


  }

  void _onMapCreated(GoogleMapController controller) {
    
    _mapController = controller;
    _centerView();
  }

   _centerView() async{
     /* var api =Provider.of<DirectionProvider>(context); */
      final LatLng fromPoint = LatLng (posicionActual.latitude,posicionActual.longitude);
     final LatLng toPoint = LatLng (widget.entidad.latitud.toDouble(),widget.entidad.longitud.toDouble(),);
    
     await _mapController.getVisibleRegion();

   /*    print('Buscando direcciones');
     await api.findDirections(fromPoint, toPoint); */

     var left = min(fromPoint.latitude,toPoint.latitude);
     var right = max(fromPoint.latitude,toPoint.latitude);
     var top = max(fromPoint.longitude,toPoint.longitude);
     var buttom = min(fromPoint.longitude,toPoint.longitude);

  /*    api.currentRoute.first.points.forEach((point){
       left = min(left, point.latitude);
       right = max(right, point.latitude);
       top = max(top, point.longitude);
       buttom = min(buttom, point.longitude);
     }); */

     var bounds = LatLngBounds(
       southwest: LatLng(left,buttom),
       northeast: LatLng(right,top)
     );


    var cameraUpdate = CameraUpdate.newLatLngBounds(bounds, 50);
     _mapController.animateCamera(cameraUpdate);

     var api =Provider.of<DirectionProvider>(context);
      print('Buscando direcciones');
     api.findDirections(fromPoint, toPoint);



   }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();

    
  }


  
}