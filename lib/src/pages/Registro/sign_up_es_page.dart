import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:lectorqr/src/pages/scope_model/AuthApi/auth_api_service.dart';
import 'package:lectorqr/src/utils/session_login.dart';
import 'package:lectorqr/src/widget/circle_login_widget.dart';
import 'package:lectorqr/src/widget/input_login_widget.dart';

class SignUpEsPage extends StatefulWidget {
  SignUpEsPage({Key key}) : super(key: key);

  @override
  _SignUpEsPageState createState() => _SignUpEsPageState();
}

class _SignUpEsPageState extends State<SignUpEsPage> {

  final _formKey = GlobalKey<FormState>();
  final _authAPI = AuthAPI();
  final _session = Session();

  var _profile = 3,_entity ='',_firstname ='', _lastname ='', _email ='', _password ='',_lang='es';
  var _isFetching = false;

   bool _obscureText = true;

   void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }


  @override
  void initState() {
    
    super.initState();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

  }

// metod que se llama cuando doy click en el boton Crear Cuenta
  _submit() async{
  
   if (_isFetching)return;

   final isValid = _formKey.currentState.validate();
   if (isValid){
     setState(() {
        _isFetching =true;
      });

     final isResponse = await _authAPI.register( context,
                                  profile: _profile,
                                  entity: _entity,
                                  lang  : _lang,
                                  firstname: _firstname, 
                                  lastname: _lastname,
                                  email: _email,
                                  password: _password);

      setState(() {
        _isFetching =false;
      });

      if (isResponse){
        _respuestaRegistro();
        
        
      }

   }
  }
  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: GestureDetector(
        onTap: (){
          // con esto se minimiza el teclado cuando doy click afuera del text
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
            width: size.width,
            height: size.height,
            child: Stack(
             children: <Widget>[

              Positioned(
              right: -size.width*0.25,
              top: -size.width*0.40,
              child: CircleLogin(
                      radius: size.width*0.45,
                      colors: [
                       Color(0xffc7c7c7).withOpacity(0.3),
                        Color(0xffc7c7c7).withOpacity(0.4),
                      ]
                    ),
            ),


             Positioned(
                right: size.width*0.05,
                top: size.width*0.55,
                child: CircleLogin(
                        radius: size.width*0.07,
                        colors: [
                          
                          Colors.blueAccent, 
                          Color(0xff2979ff).withOpacity(0.8),
                         
                          
                        ]
                      ),
              ),


              Positioned(
                right: size.width*0.80,
                top: size.width*0.45,
                child: CircleLogin(
                        radius: size.width*0.08,
                        colors: [
                          
                          Color(0xffc7c7c7).withOpacity(0.3),
                          Color(0xffc7c7c7).withOpacity(0.4),
                         
                          
                        ]
                      ),
              ),

            Positioned(
              left: -size.width*0.30,
              top: -size.width*0.40,
              child: CircleLogin(
                      radius: size.width*0.40,
                      colors: [
                        Colors.blueAccent, 
                        Color(0xff2979ff).withOpacity(0.8),
                      ]
                    ),
            ),

            SafeArea(
                    child: SingleChildScrollView(
                      child: Container(
                        width: size.width,
                        height: size.height,
                        child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    SizedBox(height: size.height * 0.030),
                                     Container(
                                        width: size.width * 0.20,
                                        height: size.height * 0.10,
                                       
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(10),
                                          image: new DecorationImage(
                                            image: new ExactAssetImage('assets/registro.png'),
                                            fit: BoxFit.cover,
                                      ),
                                        
                                      ),
                                ),

                               SizedBox(height: size.height * 0.010),


                                Text('Registrese',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 20 , fontWeight: FontWeight.w800),
                                
                                ),

                            ],
                          ),

                BounceInLeft(
                  child: Container(
                        width: size.width * 0.9,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5.0),
                            color: Color(0xff8D8D8D).withOpacity(0.2),
                            
                        ),
                        child: Card(
                          elevation: 15,
                          child: Column(
                            children: <Widget>[

                            SizedBox(height: size.height * 0.010),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Form(
                                 key: _formKey,
                                 child:Column(
                                   children: <Widget>[
                                         InputText(label: 'Ingrese su nombre',
                                                 fontSize: 20,
                                                 validator: (String text){
                                                     if (text.isNotEmpty){
                                                        _firstname = text;
                                                         return null;
                                                     }
                                                         return "Ingrese su nombre";
                                                   }),
                                               SizedBox(height: size.height * 0.010),
                                                 InputText(label: 'Ingrese su apellido',
                                                         fontSize: 20,
                                                         validator: (String text){
                                                           if (text.isNotEmpty){
                                                             _lastname = text;
                                                             return null;
                                                           }
                                                           return "Ingrese su apellido";
                                                         }),
                                               SizedBox(height: size.height * 0.010),
                                               InputText(label: 'Ingrese su Email',
                                                         fontSize: 20,
                                                         inputType: TextInputType.emailAddress,
                                                         validator: (String text){
                                                           if (text.contains("@")){
                                                             _email = text;
                                                             return null;
                                                           }
                                                           return "Email Inválido";
                                                         }),
                                               SizedBox(height: size.height * 0.010),
                                               InputText(label: 'Ingrese su contraseña',
                                                         fontSize: 20,
                                                         isSecure: _obscureText,
                                                         validator: (String text){
                                                           if (text.isEmpty) { 
                                                                return "Por favor, ingrese contraseña";
                                                            } else if(text.length < 7){
                                                                return "Debe tener más de 6 caracteres";

                                                            }

                                                              _password = text;
                                                              return null;
                                                         }),
                                                FlatButton(
                                                  onPressed: _toggle,
                                                   child: new Text(_obscureText ? "Mostrar" : "Ocultar"))
                                             ],
                                           )
                                         ),
                            ),
                            
                            
                                CupertinoButton(
                                     
                                      color: Color(0xff2962ff),
                                      borderRadius: BorderRadius.circular(10),
                                      onPressed: ()=> _submit(),
                                        child: Text('Crear cuenta',
                                                      style: TextStyle(fontSize: 20),),

                                            ),
                            
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text('¿Ya tienes una cuenta?',
                                            style: TextStyle(fontSize: 15,color: Colors.black54),),
                                        CupertinoButton(
                                          onPressed: ()=>Navigator.pop(context),
                                          child: Text('Iniciar Sesión',
                                            style: TextStyle(fontSize: 15,color: Color(0xff69409E),fontWeight: FontWeight.bold),),
                                        )

                                ],
                            ),
                            SizedBox(height: size.height * 0.030),
                               
                          ],
                            ),
                        ),
                  ),
                ),


                  ],
              ),
                      ),
                    ),
            ),

              Positioned(
                left: 15,
                top: 15,
                child: SafeArea(
                        child: CupertinoButton(
                        padding: EdgeInsets.all(15),
                        borderRadius: BorderRadius.circular(40),
                        color: Colors.black38,
                        onPressed: ()=>Navigator.pop(context),
                        child: Icon(Icons.arrow_back, 
                                    color:Colors.white),
                      ),
                )
              ),


              // SI ESTOY REALIZACION UNA PETICION A MI SERVIDOR MOSTRAME EL ACTIVITYINDICATOR
              // CASO CONTRARIO MUESTRA EL CONTAINER VACIO
             _isFetching ? Positioned.fill(child: Container(
                color: Colors.black45,
                child: Center(
                  child: CupertinoActivityIndicator(radius: 15),
                ),
              )) : Container()

          

          ],

        ),


      ),

      )
    );
  }

  void _respuestaRegistro() async {
      await showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text('Se ha registrado con exito'),
              content: Text('Recibirá un email para verificar su cuenta ', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15,color: Color(0xff000a12))),
              actions: [
                new FlatButton(
                  child: new Text("Aceptar"),
                  onPressed: () => Navigator.of(context).pop()
                ),
              ],
            ),
      );
      Map userr = await _session.getSession();
      await _authAPI.logout(userr["token_type"], userr["access_token"]);
      _session.onClosed();
      Navigator.pushReplacementNamed(context,'botones');
    }
}
 