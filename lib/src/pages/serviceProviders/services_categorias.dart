import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:lectorqr/src/models/articulos_model.dart';
import 'package:lectorqr/src/pages/scope_model/var_global.dart';

class ArticleService with ChangeNotifier {


  List<Articulo> articles = [];
  var articulos = List<Articulo>();
  final Ruta _ruta = Ruta();

  ArticleService () {
    this.getArticulos();

  }

  getArticulos () async {
    var response = await http.get(Uri.encodeFull(_ruta.rutaArticulo), headers: {
      'Content-Type': 'application/json',
      
    });
    
      var data = json.decode(response.body);
      var articulosJson = data["data"];
      for (var articulosJson in articulosJson) {
        articulos.add(Articulo.fromJson(articulosJson));
      }

      this.articles.addAll(articulos);
    
    notifyListeners();
  }



}