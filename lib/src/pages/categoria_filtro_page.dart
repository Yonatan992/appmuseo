import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lectorqr/src/models/categoria_model.dart';
import 'package:lectorqr/src/pages/articulo_cat_page.dart';
import 'package:lectorqr/src/pages/scope_model/var_global.dart';
import 'package:lectorqr/src/utils/session_login.dart';

import 'scope_model/apiService.dart';

class CategoriaFiltro extends StatefulWidget {
  CategoriaFiltro({Key key}) : super(key: key);

  @override
  _CategoriaFiltroState createState() => _CategoriaFiltroState();
}

class _CategoriaFiltroState extends State<CategoriaFiltro> {
 

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
          child: Scaffold(
            appBar: AppBar(
            
            title: Text('Lista de categorías'),
            ),
            body: Column(
              children: <Widget>[
                SizedBox(height:size.height * 0.020),
                ListTile(
                  leading: FaIcon(FontAwesomeIcons.borderAll,color:Colors.black.withOpacity(0.6), size: 30,),
                  title: Text('Seleccione categoría',
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 25,color: Color(0xff000a12))),
                  
                  ),
                SizedBox(height:size.height * 0.020),
                Expanded(child: _ListaCategoria()),
              ]

        
            ),
          )
      );
  }
}

class _ListaCategoria extends StatefulWidget {
  @override
  __ListaCategoriaState createState() => __ListaCategoriaState();
}

class __ListaCategoriaState extends State<_ListaCategoria> {
   final _apiService = ApiService();
   Map user;

   final _session = Session();
  check() async {
    final data = await _session.getSession();
    if (data != null){
      //Navigator.pushReplacementNamed(context, 'Scan');
      setState(() {
        user = data;
        return user;
      });
      
    } else {
      Navigator.pushReplacementNamed(context, 'loginEs');
    }
  }

    @override
  void initState() {
    this.check(); 
    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    return (user == null)
          ? Center(child: CircularProgressIndicator())
          :FutureBuilder(
            future:  _apiService.getCategories(user['token_type'],user['access_token']),
            builder: (BuildContext context,AsyncSnapshot <List<Category>> categorias) {
            if (categorias.connectionState == ConnectionState.done) {

              return (categorias.data == null)
              ? Center(child: CircularProgressIndicator())
              :ListView.builder(
                itemCount: categorias.data.length,
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, int i) {
                        Category categoria = categorias.data[i];
                        return _listaCategorias(i,categoria);
                      }

              );
              
              
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },

        );
  }

  _listaCategorias(i,categoria) {
    final size = MediaQuery.of(context).size;
    final cName = categoria.aliasEs;
    return Container(
      width:size.width * 0.55,
      height:size.height* 0.30,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          child: Column(
            children: <Widget>[
              _CategoryButtom(i,categoria),
              SizedBox(height: size.height * 0.010,),
              Card(
                elevation: 10,
                child: Container(
                  width:size.width,
                  height:size.height* 0.080,
                  child: Center(
                    child: Text('${cName[0].toUpperCase()}${cName.substring(1)}',
                      style: TextStyle(
                      color: Colors.black,
                      fontSize: 20.0)),
                  ),
                ),
              ),
            ],
          ),

        ),
      ),
    );

  }
}

class _CategoryButtom extends StatelessWidget {
  final Category category;
  final int index;
  _CategoryButtom(this.index,this.category);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return GestureDetector(
          onTap: () =>
                  Navigator.push(
                    context,
                  MaterialPageRoute(
                  builder: (context) => ArticuloCatPage(category.identificador))),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(15.0),
            child: Container(
              width: size.width * 0.80,
              height: size.height * 0.50,
              decoration: BoxDecoration(
                image: (category.imagen != "")
                  ? DecorationImage(
                      image: NetworkImage(Urls.URL_IMAGEN+category.imagen),
                      
                      fit: BoxFit.cover,
                                  )
                  : DecorationImage(
                    image: new ExactAssetImage('assets/categories/no-image.jpg'),
                    fit: BoxFit.cover,
                  ),
                shape: BoxShape.rectangle,),
              child: Column(
                children: <Widget>[
                  Icon(Icons.linear_scale,size: 50,color: Colors.white,),
                  SizedBox(height: size.height * 0.30,),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.white)
                    ),
                    disabledColor: Colors.amber,
                    child: Text('Explorar',
                    style:TextStyle(fontSize: 25,color:Colors.white,fontWeight: FontWeight.bold)),
                    splashColor: Colors.amber,
                    color: Colors.blueAccent.withOpacity(0.6),
                    onPressed: () =>
                        Navigator.push(
                          context,
                        MaterialPageRoute(
                        builder: (context) => ArticuloCatPage(category.identificador))),
                          ),
                ],
              ),

            ),
          ),
    );
  }


}