import 'package:animate_do/animate_do.dart';


import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lectorqr/src/widget/HeaderIdioma_widget.dart';
import 'package:lectorqr/src/widget/botonesIdiomas_widget.dart';

class ItemBoton {
  final String ruta;
  final String imegen;
  final IconData icon;
  final String texto;
  final Color color1;
  final Color color2;

  ItemBoton(this.ruta,this.imegen,this.icon, this.texto, this.color1, this.color2 );
}

class ScrollPage extends StatefulWidget {
  @override
  _ScrollPageState createState() => _ScrollPageState();
}

class _ScrollPageState extends State<ScrollPage> {

  


  

  @override
  Widget build(BuildContext context) {
    
    
    return Scaffold(
        body: PageView(
      scrollDirection: Axis.vertical,
      children: <Widget>[_pagina1(), _paginaEs(context)],
    ));

   
  }

  Widget _pagina1() {
    return Stack(
      children: <Widget>[
        _colorFondo(),
        _imagenFondo(),
        _titulo(),
      ],
    );
  }

  Widget _colorFondo() {
    return Container(
        width: double.infinity,
        height: double.infinity,
        color: Color.fromRGBO(108, 192, 218, 0.5));
  }

  Widget _imagenFondo() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Image(
        image: AssetImage('assets/brick-brickwall-brickwork-cement.jpg'),
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _titulo() {
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: Column(
        children: <Widget>[
          SizedBox(height: size.height * 0.10),
          FadeInDown(
            delay: Duration(milliseconds:200),
            child: Text('Museo QR', 
            style:GoogleFonts.lato(fontStyle: FontStyle.italic,color: Colors.white,fontSize: 60.0))),
          
          SizedBox(height: size.height * 0.30),

          Center(
            child: Column(
              children: <Widget>[

                FadeInUpBig(
            delay: Duration(milliseconds:900),
            child: FaIcon(FontAwesomeIcons.arrowCircleUp, size: 90, color: Colors.white.withOpacity(0.9) )),
          SizedBox(height: size.height * 0.010),
         FadeInUp(
           delay: Duration(milliseconds:900),
           child: Text('Desliza hacia arriba', 
               style: GoogleFonts.oswald(fontStyle: FontStyle.italic,color: Colors.white,fontSize: 40.0),
               ),
         ),


              ],)
          )

          
     
        ],
      ),
    );
  }

  Widget _paginaEs(BuildContext context) {

    final items = <ItemBoton>[
  new ItemBoton('botones','assets/españa.png',FontAwesomeIcons.chevronCircleRight, 'Español', Color(0xffF2D572), Color(0xffFF8C01) ),
  new ItemBoton('buttons','assets/inglaterra.png',FontAwesomeIcons.chevronCircleRight, 'Inglés', Color(0xff66A9F2), Color(0xff536CF6) ),
 
];
List<Widget> itemMap = items.map(
  (item) => FadeInLeft(
      child: BotonesIdiomas(
      ruta: item.ruta,
      image: item.imegen,
      icon: item.icon,
      texto: item.texto,
      color1: item.color1,
      color2: item.color2,

    ),
  )
  ).toList();
    

    return Scaffold(
      
      body: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top:200),
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                SizedBox(height: 70,),
                //operador spread, similar a javascript
                ...itemMap

                
              ],
            ),
          ),

          HeaderIdioma(
            icon: FontAwesomeIcons.globeAmericas,
            titulo: 'Elegí tu lenguaje',
            subtitulo: 'I chose your language',
            color1: Color(0xff536CF6),
            color2: Color(0xff66A9F2),)
        ],
      )
     
    );
    
  }
 
 
}

