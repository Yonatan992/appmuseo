import 'package:flutter/material.dart';
import 'package:lectorqr/src/pages/serviceProviders/services_categorias.dart';
import 'package:lectorqr/src/widget/lista_articulo_widget.dart';

import 'package:provider/provider.dart';

class Tabs1Page extends StatefulWidget {


  @override
  _Tabs1PageState createState() => _Tabs1PageState();
}

class _Tabs1PageState extends State<Tabs1Page> {
  
  @override
  Widget build(BuildContext context) {
    final articulos = Provider.of<ArticleService>(context).getArticulos();
    return Scaffold(
      body: ListaAticulos(articulos:articulos),

    );
  }
}