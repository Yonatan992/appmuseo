import 'package:scoped_model/scoped_model.dart';

class Urls {
  static const BASE_API_URL = "https://museo.carlosdev.com.ar/";
  static const HOST_API_URL = "https://museo.carlosdev.com.ar/api";
  static const URL_ENTIDAD = "api/entities";
  static const URL_COMMENTS = "api/articlesComments";
  static const URL_COMENTARIOS = "api/comments";
  static const URL_CATEGORIAS = "api/categories";
  static const URL_ARTCAT = "/articles";
  static const URL_RESEND = "/resend";
  static const URL_LOGOUT = "/auth/logout";
  static const URL_RESETPASS = "password/email";
  static const URL_ARTICULO = "api/articles";
  static const URL_MEJORESV = "api/valorados";
  static const URL_USERS = "api/users";
  static const URL_IMAGEN = "https://museo.carlosdev.com.ar/img/";
  static const URL_ENTIDAD_SEARCH = "api/entity/search/";
}

class Grant {
  static const GRANT_TYPE = "password";
  static const GRANT_REFRESH = "refresh_token";
  static const CLIENT_ID = 2;
  static const CLIENT_SECRET ="HQCAN8fMCVbtLkOggP3jhZkHdytXTA8HR2BHzhOw";
}

class Ruta extends Model {
  String _ruta = "https://museo.carlosdev.com.ar/";
  String _rutaArticulo = "api/articles";
  String _rutaImagen = "img/";
  String _rutaEntidades = "api/entities";

  String get rutaArticulo {
    return _ruta + _rutaArticulo;
  }

  String get rutaImagen {
    return _ruta + _rutaImagen;
  }

  String get rutaEntidades {
    return _ruta + _rutaEntidades;
  }
}
