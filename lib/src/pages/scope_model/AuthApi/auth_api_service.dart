import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:lectorqr/src/pages/scope_model/var_global.dart';
import 'package:lectorqr/src/utils/dialogs_login_es.dart';
import 'package:meta/meta.dart' show required;
import 'package:lectorqr/src/utils/session_login.dart';

class AuthAPI {

final _session = Session();

Future <bool> register( BuildContext context,
              {@required int profile,
              @required String entity,
              @required String lang,
              @required String firstname,
              @required String lastname,
              @required String email,
              @required String password}) async{

  try {
        final url = "${Urls.HOST_API_URL}/signup";

        final response = await http.post(url, 
                                body: jsonEncode({
                                  "profile_id"           : profile,
                                  "entity_id"            : entity,
                                  "lang"                 : lang,
                                  "firstname"            : firstname,
                                  "lastname"             : lastname,
                                  "email"                : email,
                                  "password"             : password}),
                                headers: _setHeaders());

        // response.body es la respuesta del servidor                
        //final responseString =response.body;

        final parsed = jsonDecode(response.body);
        
        if (response.statusCode == 200) {
          final token = parsed['access_token'] as String;
          final typeToken = parsed['token_type'] as String;
          final expires = parsed['expires_at'] as String;
          final userId = parsed['user']['id'] as int;
          final userName = parsed['user']['firstname'] as String;
          final userSurname =parsed['user']['lastname'] as String;
          final userVerify =parsed['user']['verified'] as String;
          print(response.body);

          await _session.set(userId,userName,userSurname,userVerify,token,typeToken,expires);
          return true;


        } else if (response.statusCode == 422){
          
          throw PlatformException(code: "422", message: parsed['error']['email'][0]);
        }

        throw PlatformException(code: "201", message:"Error /register");

        }on PlatformException catch (e) {

          print("Error ${e.code} : ${e.message}");

          Dialogs.alert(context,title: "Vuelva a intentarlo", message:e.message);

          return false;
        }

}

Future <bool> login( BuildContext context,
              {@required String email,
              @required String password,
              }) async{

  try {
        final url = "${Urls.HOST_API_URL}/auth/login";

        final response = await http.post(url, 
                                body: jsonEncode({
                                  "email"    : email,
                                  "password" : password,
                                  }),
                                headers: _setHeaders());

        // response.body es la respuesta del servidor                
        //final responseString =response.body;

        final parsed = jsonDecode(response.body);
        
        if (response.statusCode == 200) {
          final token = parsed['access_token'] as String;
          final typeToken = parsed['token_type'] as String;
          final expires = parsed['expires_at'] as String;
          final userId = parsed['user']['id'] as int;
          final userName = parsed['user']['firstname'] as String;
          final userSurname =parsed['user']['lastname'] as String;
          final userVerify =parsed['user']['verified'] as String;
          print(response.body);

          await _session.set(userId,userName,userSurname,userVerify,token,typeToken,expires);
          return true;


        } else if (response.statusCode == 401){
          throw PlatformException(code: "401", message: parsed["message"]);
        }

        throw PlatformException(code: "201", message:"Error /register");

        }on PlatformException catch (e) {

          print("Error ${e.code} : ${e.message}");

          Dialogs.alert(context,title: "ERROR", message:e.message);

          return false;
        }


    }
  getResendEmail(int user ) async {
      final _urlResend = '${Urls.BASE_API_URL}${Urls.URL_USERS}/$user${Urls.URL_RESEND}';
      final response = await http.get(Uri.encodeFull(_urlResend));
      if (response.statusCode == 200) {
        return true;
      }
      

  }


  Future <bool> resetPass( BuildContext context,
              {@required String email,
              }) async{

        final url = "${Urls.HOST_API_URL}/${Urls.URL_RESETPASS}";

        final response = await http.post(url, 
                                body: jsonEncode({
                                  "email"    : email,
                                  }),
                                headers: _setHeaders());
        
        if (response.statusCode == 200) {
          return true;

        }

        return false; 

    }


    Future<bool> logout(String _typeToken,String _token) async {
    
        final _urlLogout = '${Urls.HOST_API_URL}${Urls.URL_LOGOUT}';
        //final response = await http.get(Uri.encodeFull(_urlCategories));

        final response = await http.get(_urlLogout,headers: _setHeadersToken(_typeToken,_token));

        if (response.statusCode == 200) {
          print('Cerro Sesion !!!!');
          return true;
          
        } 
        return false;
    }

//cabecera ==> headers de la app
  _setHeaders() => {
        'Content-type' : 'application/json',
        'Accept' : 'application/json',
  };

  _setHeadersToken(String _typeToken,String _token) => {
        'Content-type' : 'application/json',
        'Accept' : 'application/json',
        'Authorization' :_typeToken+' ' +_token,
  };





}
