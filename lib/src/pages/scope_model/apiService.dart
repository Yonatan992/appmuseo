import 'dart:convert';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:lectorqr/src/models/articulos_model.dart';
import 'package:lectorqr/src/models/categoria_model.dart';
import 'package:lectorqr/src/models/entidad_model.dart';

import 'package:lectorqr/src/pages/scope_model/var_global.dart';
import 'package:lectorqr/src/utils/dialogs_login_es.dart';

List data;
List valorados;
Articulo articulo;
Category categoria;

class ApiService {

  static Future<bool> getEntitiesArt(int idEntidad) async {
    //return await _get('${Urls.BASE_API_URL}${Urls.URL_ENTIDAD}/$idEntidad/articles/');
        final _urlArt = '${Urls.BASE_API_URL}${Urls.URL_ENTIDAD}/$idEntidad/articles/';
        final response = await http.get(Uri.encodeFull(_urlArt));
  
        if (response.statusCode == 200) {
        final decodedData = json.decode(response.body);
        data = decodedData["data"];
         print(data);
        } else {
            throw Exception('Error al cargar la publicación');
          }
       
        return true;
  }

    Future<Articulo> scanArt(BuildContext context, String urlScan) async {
        //return await _get('$urlArt');
      
          final _urlS = '${Urls.BASE_API_URL}${Urls.URL_ARTICULO}/$urlScan';
          final response = await http.get(Uri.encodeFull(_urlS));
          final decodedData = await json.decode(response.body);
          

          if (response.statusCode == 200) {
          //final articulos = new Articulos.fromJsonList(decodedData['data']);
          //print(articulos.items[0].tituloEn);
           //return articulos.items[0];
            articulo = new Articulo.fromJson(decodedData['data'][0]);
            print(articulo.comments);
            return articulo;

          } else {
            return articulo;
          }
          
          

      
    }
        

  static Future<bool> buscarEntidad(String query) async {
        //return await _get('${Urls.BASE_API_URL}${Urls.URL_ENTIDAD_SEARCH}$query');
        final url = '${Urls.BASE_API_URL}${Urls.URL_ENTIDAD_SEARCH}$query';
        final response = await http.get(Uri.encodeFull(url));
        final decodedData = json.decode(response.body);

         if (response.statusCode == 200) {
           data = decodedData['data'];
        //print(decodedData);
        return true;
         }else{
           return false;
         }
        
  }

  Future <List<Entidad>> getEntidades() async{
          final _urlS = '${Urls.BASE_API_URL}${Urls.URL_ENTIDAD}';
          final responseData = await http.get(Uri.encodeFull(_urlS));
          final dataEntidades = json.decode(responseData.body);
          final entidades = new Entidades.fromJsonList(dataEntidades['data']);
          
          return entidades.items;

  }

  Future <bool> storeComentario( BuildContext context,
              {@required int articulo,
              @required int usuario,
              @required int puntuacion,
              @required String comentario,
             }) async{

        try {
              final url = "${Urls.HOST_API_URL}/comments";

              final response = await http.post(url, 
                                      body: jsonEncode({
                                        "articulo"    : articulo,
                                        "usuario"     : usuario,
                                        "puntuacion"  : puntuacion,
                                        "detalle"     : comentario,
                                      }),
                                      headers: _setHeaders());

            
              final parsed = jsonDecode(response.body);
              
              if (response.statusCode == 200) {
                print(response.body);
                return true;

              } else if (response.statusCode == 422){
                throw PlatformException(code: "422", message: parsed["error"]);
              }

              throw PlatformException(code: "201", message:"Error");

              }on PlatformException catch (e) {

                print("Error ${e.code} : ${e.message}");

                Dialogs.alert(context,title: "ERROR", message:e.message);

                return false;
              }

            }
      
     Future<List> mejoresValorados() async {
        //return await _get('${Urls.BASE_API_URL}${Urls.URL_ENTIDAD_SEARCH}$query');
        final url = '${Urls.BASE_API_URL}${Urls.URL_MEJORESV}';
        final response = await http.get(Uri.encodeFull(url));
        final decodedData = json.decode(response.body);
        valorados = decodedData["data"];
        print(valorados);
        return valorados;
  }


    Future<bool> deleteComment(int comment) async {
    
        final _urlComment = '${Urls.BASE_API_URL}${Urls.URL_COMENTARIOS}/$comment';
        final response = await http.delete(Uri.encodeFull(_urlComment));
  
        if (response.statusCode == 200) {
        final decodedData = json.decode(response.body);
        //data = decodedData['data'][0];
         print(decodedData);
         return true;
        } else {
            return false;
          }
    }


    Future<bool> updateComment(int idComment,BuildContext context,
        {
        @required int puntuacion,
        @required String comentario,
             }) async { 
                final _urlComment = '${Urls.BASE_API_URL}${Urls.URL_COMENTARIOS}/$idComment';
                final response = await http.put(_urlComment, 
                                      body: jsonEncode({
                                        "puntuacion"  : puntuacion,
                                        "detalle"     : comentario,
                                      }),
                                      headers: _setHeaders());
              if (response.statusCode == 200) {
                // If the server did return a 200 OK response,
                // then parse the JSON.
                //return Comment.fromJson(json.decode(response.body));
                final decodedData = json.decode(response.body);
                print(decodedData);
                return true;
              } else {
                // If the server did not return a 200 OK response,
                // then throw an exception.
                throw Exception('Error al actualizar su comentario');
                
              }
    }


    Future<List<Category>> getCategories(String _typeToken, String _token) async {
    
        final _urlCategories = '${Urls.BASE_API_URL}${Urls.URL_CATEGORIAS}';
        //final response = await http.get(Uri.encodeFull(_urlCategories));

        final response = await http.get(_urlCategories,headers: _setHeadersToken(_typeToken,_token));

        var categorias = List<Category>();
        if (response.statusCode == 200) {
          final decodedData = json.decode(response.body);
          var categoriasJson = decodedData["data"];
            for (var categoriasJson in categoriasJson) {
              categorias.add(Category.fromJson(categoriasJson));
            }
            
        } 
        return categorias;
    }


    Future<List<Articulo>> getArticulosForCategory(int category) async {
    
        final _urlCategories = '${Urls.BASE_API_URL}${Urls.URL_CATEGORIAS}/$category${Urls.URL_ARTCAT}';
        final response = await http.get(Uri.encodeFull(_urlCategories));
        var articulos = List<Articulo>();
        if (response.statusCode == 200) {
          final decodedData = json.decode(response.body);
          var articulosJson = decodedData["data"];
            for (var articulosJson in articulosJson) {
              articulos.add(Articulo.fromJson(articulosJson));
            }
            
        } 
        return articulos;
    }

    


//cabecera ==> headers de la app
  _setHeaders() => {
        'Content-type' : 'application/json',
        'Accept' : 'application/json',
  };

  _setHeadersToken(String _typeToken, String _token) => {
        'Content-type' : 'application/json',
        'Accept' : 'application/json',
        'Authorization' :_typeToken+' '+_token,
  };






}
