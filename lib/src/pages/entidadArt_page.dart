import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
//import 'package:lectorqr/src/pages/scope_model/var_global.dart';
import 'package:lectorqr/src/pages/scope_model/apiService.dart';

class EntidadesArtPage extends StatefulWidget {
  final int _data;
  //final Ruta _ruta = Ruta();
  EntidadesArtPage(this._data);

  @override
  _EntidadesArtPageState createState() => _EntidadesArtPageState();
}

class _EntidadesArtPageState extends State<EntidadesArtPage> {
  ScrollController _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  _scrollToTop() {
    _scrollController.animateTo(_scrollController.position.minScrollExtent,
        duration: Duration(milliseconds: 2000), curve: Curves.easeIn);
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Lista de Catálogos'),
      ),
      
      body: FutureBuilder(
        future: ApiService.getEntitiesArt(widget._data),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            
            return Center(
              child: (data.length == 0)
                    ? Container(
                      width: size.width,
                      height: size.height * 0.30,
                      child: Card(
                        elevation: 15,
                      child: Center(child: Column(
                        children: <Widget>[
                          Text('El museo no posee artículos publicados',
                              style: TextStyle(
                              fontSize: 15.0)),
                          SizedBox(height:size.height * 0.030),
                          FaIcon(FontAwesomeIcons.exclamationCircle,color:Colors.black.withOpacity(0.6), size: 80,),
                        ],
                      ))),
                    )
              :ListView.separated(
                controller: _scrollController,
                separatorBuilder: (context, i) {
                  return Divider(
                    height: 2,
                    color: Colors.black,
                  );
                },
                itemCount: data == null ? 0 : data.length,
                itemBuilder: (context, i) {
                  return Card(
                    elevation: 20.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    color:  Color(0xff2979ff).withOpacity(0.6),
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 30.0),
                        Text(data[i]["denominacionEnt"] == null ? 'Cargando ..' : data[i]["denominacionEnt"],
                            textAlign: TextAlign.center,
                            overflow: TextOverflow.ellipsis,
                            style:GoogleFonts.montserrat(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))),
                        Text(data[i]["tituloEs"] == null ? 'Cargando ..' : data[i]["tituloEs"],
                            textAlign: TextAlign.center,
                            overflow: TextOverflow.ellipsis,
                           style:GoogleFonts.bevan(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))),
                        SizedBox(height: 30.0),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(5.0),
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal:20),
                            width: 350,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                color:  Color(0xfffafafa),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                      color: Color(0xfffafafa),
                                      blurRadius: 8.0,
                                      spreadRadius: 2.0,
                                      offset: Offset(2.0, -10.0))
                                ]),
                              child: Column(
                                children: <Widget>[
                                  ClipRRect(
                                      borderRadius: BorderRadius.circular(15.0),
                                      child: (data[i]["imagen"] != null) 
                                              ?FadeInImage(
                                                image: NetworkImage(data[i]["imagen"]),
                                                width: MediaQuery.of(context).size.width,
                                                placeholder: AssetImage('assets/giphy.gif'),
                                                fadeInDuration: Duration(milliseconds: 700),
                                                height: 250.0,
                                                fit: BoxFit.cover,
                                          )
                                          : Image(image:AssetImage('assets/no-image.png'),)
                                    ),
                                  SizedBox(height: 10.0),

                                  Text('Categoría: ' + data[i]["denominacionCat"] == null ? 'Cargando ..' : data[i]["denominacionCat"],
                                      textAlign: TextAlign.right,
                                      style:GoogleFonts.bevan(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 20))),
                                  SizedBox(height: 20.0),
                                  Text(data[i]["descripcionEs"] == null ? 'Cargando ..' : data[i]["descripcionEs"],
                                      textAlign: TextAlign.justify,
                                       style:GoogleFonts.lato(textStyle: TextStyle(color: Colors.black, letterSpacing: .5,fontSize: 15))),
                                  SizedBox(height: 40.0),
                                ],
                              ),
                            
                          ),
                        ),
                        SizedBox(height: 40.0),
                      ],
                    ),
                  );
                },
              ),
            );
          }  else if (snapshot.hasError) {
                return Center(
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 60.0),
                      Text(
                        "${snapshot.error}",
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black87,
                              fontSize: 40.0))
                    ],
                  ),
                );
            }
          return Center(
            child: Column(
              children: <Widget>[
                CircularProgressIndicator(),
                SizedBox(height: 30.0),
                Text('Por favor espera...')
              ],
            ),
            
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: _scrollToTop,
          child: Icon( Icons.arrow_upward),
      )
    );
  }
}
