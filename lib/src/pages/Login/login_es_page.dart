import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:lectorqr/src/pages/scope_model/AuthApi/auth_api_service.dart';
import 'package:lectorqr/src/utils/session_login.dart';
import 'package:lectorqr/src/widget/circle_login_widget.dart';
import 'package:lectorqr/src/widget/input_login_widget.dart';

class LoginEsPage extends StatefulWidget {
  LoginEsPage({Key key}) : super(key: key);

  @override
  _LoginEsPageState createState() => _LoginEsPageState();
}

class _LoginEsPageState extends State<LoginEsPage> {

  final _formKey = GlobalKey<FormState>();
  final _authAPI = AuthAPI();

  var _email = '', _password ='';
  var _isFetching = false;

  bool _obscureText = true;

   void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  void initState() {
    
    super.initState();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    check();

  }

  final _session = Session();
   check() async {
    final data = await _session.getSession();
    if (data != null){
      
       Navigator.pushNamedAndRemoveUntil(context,'botones',(_)=>false);
    } 
  }

  _submit() async {

    if (_isFetching)return;

    final isResponse = _formKey.currentState.validate();
    if (isResponse){
      setState(() {
        _isFetching =true;
      });

      final isOk = await _authAPI.login(context, 
                                  email: _email, 
                                  password: _password,);
      
      setState(() {
        _isFetching =false;
      });

      if(isOk){
        Navigator.pushReplacementNamed(context, 'Scan');
        
      }

    }
  }
  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: GestureDetector(
        onTap: (){
          // con esto se minimiza el teclado cuando doy click afuera del text
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
            width: size.width,
            height: size.height,
            child: Stack(
             children: <Widget>[

              Positioned(
              right: -size.width*0.20,
              top: -size.width*0.55,
              child:FadeInLeftBig(
                child: CircleLogin(
                        radius: size.width*0.45,
                        colors: [
                          
                          Color(0xffc7c7c7).withOpacity(0.3),
                          Color(0xffc7c7c7).withOpacity(0.4),
                         
                          
                        ]
                      ),
              ),
            ),


              Positioned(
              right: size.width*0.05,
              top: size.width*0.65,
              child: CircleLogin(
                      radius: size.width*0.07,
                      colors: [
                        
                        Colors.blueAccent, 
                        Color(0xff2979ff).withOpacity(0.8),
                       
                        
                      ]
                    ),
            ),


            Positioned(
              right: size.width*0.80,
              top: size.width*0.45,
              child: CircleLogin(
                      radius: size.width*0.08,
                      colors: [
                        
                        Color(0xffc7c7c7).withOpacity(0.3),
                        Color(0xffc7c7c7).withOpacity(0.4),
                       
                        
                      ]
                    ),
            ),

              Positioned(
              left: -size.width*0.22,
              top: -size.width*0.38,
              child: CircleLogin(
                      radius: size.width*0.40,
                      colors: [
                        Colors.blueAccent, 
                        Color(0xff2979ff).withOpacity(0.8),
                      ]
                    ),
            ),

              SafeArea(
                      child: SingleChildScrollView(
                        child: Container(
                          width: size.width,
                          height: size.height,
                          child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                       Container(
                                          width: size.width * 0.25,
                                          height: size.height * 0.15,
                                          margin: EdgeInsets.only(top: size.width*0.2),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(10),
                                            image: new DecorationImage(
                                              image: new ExactAssetImage('assets/login.png'),
                                              fit: BoxFit.cover,
                                        ),
                          
                                        ),
                                  ),
                            SizedBox(height: size.height * 0.030),

                                  Text('Iniciar Sesión',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 30 , fontWeight: FontWeight.w800),
                                  
                                  ),

                              ],
                            ),

                BounceInLeft(
                  child: Container(
                          
                          width: size.width * 0.9,
                          
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              color: Color(0xff8D8D8D).withOpacity(0.2),
                              
                          ),
                          child: Card(
                            elevation: 15,
                            child: Column(
                                children: <Widget>[
                                        SizedBox(height: 5,),
                                         Container(
                                           child: Padding(
                                             padding: const EdgeInsets.all(8.0),
                                             child: Form(
                                                key: _formKey,
                                                child:Column(
                                                  children: <Widget>[
                                                    InputText(label: 'Ingrese su Email',
                                                              fontSize: 20,
                                                              inputType: TextInputType.emailAddress,
                                                              validator: (String text){
                                                                if (text.contains("@")){
                                                                  _email = text;
                                                                  return null;
                                                                }
                                                                return "Email Inválido";
                                                              }),
                                                    SizedBox(height: size.height * 0.020),
                                                    InputText(label: 'Ingrese Contraseña',
                                                              fontSize: 20,
                                                              isSecure: _obscureText,
                                                              validator: (String text){
                                                                if (text.isEmpty) { 
                                                                  return "Por favor, ingrese contraseña";
                                                                } else if(text.length < 7){
                                                                  return "Debe tener más de 6 caracteres";

                                                                }

                                                                _password = text;
                                                                return null;
                                                                
                                                              }),
                                                    Row(
                                                      children: <Widget>[
                                                        FlatButton(
                                                        
                                                           onPressed: _toggle,
                                                            child: new Text(_obscureText ? "Mostrar" : "Ocultar")),
                                                            Icon(Icons.remove_red_eye),
                                                      ],
                                                    ),
                                                     CupertinoButton(
                                            onPressed: ()=>Navigator.pushNamed(context, 'resetearPassEs'),
                                            child: Text('¿ Se ha olvidado su contraseña ?',
                                              style: TextStyle(fontSize: 15,color: Color(0xff2962ff),fontWeight: FontWeight.normal),),
                                          ),
                                        CupertinoButton(
                                                
                                                color:  Color(0xff2962ff),
                                                borderRadius: BorderRadius.circular(10),
                                                onPressed: ()=> _submit(),
                                                child: Text('Iniciar Sesión',
                                                        style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),

                                              ),

                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text('¿ Eres Nuevo Aquí ?',
                                              style: TextStyle(fontSize: 16,color: Colors.black54,fontWeight: FontWeight.bold),),
                                          CupertinoButton(
                                            onPressed: ()=>Navigator.pushNamed(context, 'singUpEs'),
                                            child: Text('Registrate',
                                              style: TextStyle(fontSize: 20,color: Color(0xff69409E),fontWeight: FontWeight.bold),),
                                          )

                                        ],
                                    ),
                                                    
                                  ],
                                )
                              ),
                            ),
                          ),

                                  ],
                                ),
                          ),
                    ),
                ),
                  


              ],
                    ),
                        ),
                      ),
                  ),

              // SI ESTOY REALIZACION UNA PETICION A MI SERVIDOR MOSTRAME EL ACTIVITYINDICATOR
              // CASO CONTRARIO MUESTRA EL CONTAINER VACIO
              _isFetching ? Positioned.fill(child: Container(
                     color: Colors.black45,
                      child: Center(
                              child: CupertinoActivityIndicator(radius: 15),
                      ),
                )) : Container()
          
          ],

        ),


      ),

      )
    );
  }
}
 