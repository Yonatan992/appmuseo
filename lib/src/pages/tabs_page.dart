import 'package:flutter/material.dart';
import 'package:lectorqr/src/pages/tabs1_page.dart';

import 'package:provider/provider.dart';

class TabsPage extends StatefulWidget {
  TabsPage({Key key}) : super(key: key);

  @override
  _TabsPageState createState() => _TabsPageState();
}

class _TabsPageState extends State<TabsPage> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_)=> new _NavegacionModel(),
          child: Scaffold(
        body: _Paginas(),
        bottomNavigationBar: _Navegacion(),

      ),
    );
  }
}

class _Navegacion extends StatelessWidget {
 
 
  @override
  Widget build(BuildContext context) {
    //de esta manera busco la instancia de NavegacionModel a traves de mi arbolde Widget
    final navegacioModel = Provider.of<_NavegacionModel>(context);

    return BottomNavigationBar(
      //estoy escuchando el valor de la pagina actual
      currentIndex: navegacioModel.paginaActual,
      // y aqui lo estoy cambiando
      onTap: (i) => navegacioModel.paginaActual = i,
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.person_outline),title: Text('Articulos')),
        BottomNavigationBarItem(icon: Icon(Icons.public),title: Text('Categorias')),
      ]
      );
  }
}

class _Paginas extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final navegacioModel = Provider.of<_NavegacionModel>(context);

    return PageView(
      controller: navegacioModel._pageController,
      //physics: BouncingScrollPhysics(),
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
          Tabs1Page(),
          Container(
          color: Colors.blue,
        ),
      ],
    );
  }
}


class _NavegacionModel with ChangeNotifier {

  int _paginaActual =0;
  PageController _pageController = new PageController();

  int get paginaActual => this._paginaActual;

  set paginaActual (int valor){
    this._paginaActual = valor;

    _pageController.animateToPage(valor, duration: Duration(milliseconds: 250), curve: Curves.easeInOut);
    notifyListeners();
  }

  PageController get pageContrller => this._pageController;
}