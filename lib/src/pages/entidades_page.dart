import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:lectorqr/src/models/entidad_model.dart';
import 'package:lectorqr/src/pages/entidadArt_page.dart';
import 'package:lectorqr/src/pages/maps_museos_page.dart';
//import 'package:lectorqr/src/pages/scope_model/apiService.dart';
import 'package:lectorqr/src/pages/scope_model/var_global.dart';
//import 'package:lectorqr/src/pages/ubacion_museo_page.dart';
import 'package:lectorqr/src/search/search_delegate.dart';
import 'package:lectorqr/src/utils/dialogs_login_es.dart';
import 'package:lectorqr/src/utils/session_login.dart';
import 'package:lectorqr/src/widget/menu_widget.dart';

class EntidadesPages extends StatefulWidget {
  
  _EntidadesPagesState createState() => _EntidadesPagesState();
}

class _EntidadesPagesState extends State<EntidadesPages> with AutomaticKeepAliveClientMixin {
  final Ruta _ruta = Ruta();
  List data;
  ScrollController _scrollController;

  Future<String> getData() async {
    
    var response =
    
        await http.get(Uri.encodeFull(_ruta.rutaEntidades), headers: {
      "Accept": "application/json",
    });

    this.setState(() {
      var extraerData = json.decode(response.body);
      data = extraerData["data"];
    });
    return "Success!";
  }

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
    this.getData();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  _scrollToTop() {
    _scrollController.animateTo(_scrollController.position.minScrollExtent,
        duration: Duration(milliseconds: 2000), curve: Curves.easeIn);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Museos'),
        actions: <Widget>[
          IconButton(
            icon: FaIcon(FontAwesomeIcons.search,color: Colors.black,),
            onPressed: () {
              showSearch(context: context, delegate: DataSearch());
            },
          )
        ],
      ),
      drawer: MenuWidget(),
      body:Stack(
        children: <Widget>[
          _crearCard(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: _scrollToTop,
          child: Icon( Icons.arrow_upward),
        )
    );
  }

  Widget _crearCard() {
    return (data == null)
          ? Center(child: CircularProgressIndicator())
          :Card(
        child: ListView.builder(
            controller: _scrollController,
            itemCount: data == null ? 0 : data.length,
            itemBuilder: (BuildContext context, int i) {
              
                return Card(
                  elevation:15,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10,),
                      Container(
                        margin: EdgeInsets.symmetric(vertical:10),
                        width: 400,
                        decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  gradient: LinearGradient(colors:[
                                        Color(0xffBDBDBD).withOpacity(0.2),
                                        Color(0xff8D8D8D).withOpacity(0.2),
                                          
                                  ]),
                          ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(3.0),
                                    child: Column( 
                                    children: <Widget>[
                                      
                                  Center(
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.only(topLeft:Radius.circular(20),topRight:Radius.circular(20) ),
                                      child: (data[i]["imagen"] != null) 
                                          ? FadeInImage(
                                        image: NetworkImage(data[i]["imagen"]),
                                        placeholder: AssetImage('assets/carga.gif'),
                                        fadeInDuration: Duration(milliseconds: 500),
                                        width: 400,
                                        height:250,
                                        fit: BoxFit.cover,
                                      )
                                      : Image(image:AssetImage('assets/no-image.png'),)
                                    ),
                                  ),
                                  SizedBox(height: 20,),
                                  Container(
                                    width: 380, 
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20.0),
                                      color:  Color(0xff2962ff),
                                      ),
                                    child: Column(
                                      children: <Widget>[
                                        ListTile(
                                          leading: FaIcon(FontAwesomeIcons.searchLocation,color:Colors.white.withOpacity(0.6), size: 40,),
                                          title: Text(data[i]["denominacion"] == null ? 'Cargando ..' : data[i]["denominacion"], style:TextStyle(fontSize: 20,color:Color(0xffF8F5F0),fontWeight: FontWeight.bold)),
                                          subtitle: Column(
                                            children: <Widget>[
                                              Text('Dirección: ' + data[i]["ubicacion"] + ' - '+data[i]["denominacionL"] == null ? 'Cargando ..' : data[i]["ubicacion"] + ' - '+data[i]["denominacionL"] , style:TextStyle(fontSize: 18,color:Color(0xffF8F5F0),fontWeight: FontWeight.normal)),
                                              Row(
                                                children: <Widget>[
                                                  FaIcon(FontAwesomeIcons.mapMarked,color:Colors.white.withOpacity(0.6), size: 40,),
                                                  SizedBox(width: 20,),
                                                  Text('Cómo llegar',style:TextStyle(fontSize: 18,color:Color(0xffF8F5F0),fontWeight: FontWeight.normal)),
                                                ],
                                              ),
                                            ],
                                          ),
                                          onTap: () =>_ubicacionMuseo(data[i]),
                                        ),
                                        
                                      ],
                                    ),
                                  ),

                                  
                                SizedBox(height: 20,),
                                  Container(
                                    width: 380, 
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20.0),
                                      color: Color(0xff2962ff),
                                      ),
                                    child: RawMaterialButton(
                                      onPressed: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) => EntidadesArtPage(data[i]['identificador'])));
                                          },
                                      child: ListTile(
                                          leading: FaIcon(
                                            FontAwesomeIcons.chevronCircleRight,
                                            color:Colors.white.withOpacity(0.6), size: 40,),
                                          title: Text('Ver artículos',
                                              style: TextStyle(
                                                  color: Color(0xffF8F5F0),
                                                  fontSize: 20.0)),
                                           ),
                                    ),
                                  ),
                                  SizedBox(height: 20,),
                                  
                                ],
                              ),
                            ),
                      ),
                      Divider()
                    ],
                  ),
                );
              
            }
          )
        );
  }

  _ubicacionMuseo (dataParametro)async{
    final _session = Session();
    Map userr = await _session.getSession();
    if (userr != null && userr['user_verify'] == '1'){

      Entidad dataEntidad = new Entidad.fromJson(dataParametro);
      Navigator.push(
        context,
        MaterialPageRoute(
        builder: (context) => MapsMuseos(entidad: dataEntidad)),
      );
    
    }else if (userr != null && userr['user_verify'] == '0'){
      Dialogs.alert(context,title: "Servicio de ubicación", message:'Por favor, Confirme su email');
    }else {
      Dialogs.alert(context,title: "Servicio de ubicación", message:'Por favor, Inicie sesion');
    }
   
  }

  @override
  bool get wantKeepAlive => true;

}
