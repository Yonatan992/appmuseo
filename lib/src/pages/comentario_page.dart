import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lectorqr/src/models/articulos_model.dart';
import 'package:lectorqr/src/pages/scope_model/apiService.dart';
import 'package:lectorqr/src/pages/showScan_page.dart';
import 'package:lectorqr/src/utils/session_login.dart';
import 'package:rating_bar/rating_bar.dart';

class ComentarioPage extends StatefulWidget {
   final Articulo articulo;
  ComentarioPage({this.articulo});



  @override
  _ComentarioPageState createState() => _ComentarioPageState();
}

class _ComentarioPageState extends State<ComentarioPage> {

  final _apiService = ApiService();
  Map user;


   final _formKey = GlobalKey<FormState>();
   String _comentario ='';
  double _ratingStar = 0.0;
  var _isFetching = false;


   final _session = Session();
  check() async {
    Map data = await _session.getSession();
    if (data != null){
      //Navigator.pushReplacementNamed(context, 'Scan');
      setState(() {
        user = data;
        return user;
      });
      
    } else {
      Navigator.pushReplacementNamed(context, 'loginEs');
    }
  }


    @override
  void initState() {
    this.check(); 
    super.initState();

  }


  @override
  void dispose() {
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    

    return Container(
       child: Scaffold(
         appBar: AppBar(        
        centerTitle: true,
        brightness: Brightness.light,
        title: Text('Comentario',
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontWeight: FontWeight.bold)),
        backgroundColor: Colors.blueAccent,
      ),

      body: ListView(
        
        children: <Widget> [
          _headerComentario(),
          _crearComentario(),

        ]
      )
    ),

    
    );
  }

  Widget _crearComentario() {

    return Stack( 
      children:<Widget>[
        Container(
        child: Card(
          elevation: 15,
          shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
          child: Column(
              children: <Widget>[
                Container(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[

                        ListTile(
                          leading: FaIcon(FontAwesomeIcons.star,color:Colors.black.withOpacity(0.6), size: 20,),
                          title: Text('Valoración :   $_ratingStar',
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontWeight: FontWeight.normal,fontSize: 20,color: Color(0xff000a12))),
                          subtitle: RatingBar(
                          
                            onRatingChanged: (rating) => setState(() =>  _ratingStar = rating,), 
                            filledIcon: Icons.star,
                            filledColor: Colors.yellow,
                            emptyIcon: Icons.star_border,
                        ),
                        ),
                        SizedBox(height: 20),

                        Text('Comparte detalle sobre tu experiencia con el artículo Scaneado',style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10.0,color:Color(0xff000a12))),
                        
                        Padding(
                            padding: EdgeInsets.all(8.0),
                            child: TextFormField(
                                decoration:  InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                counter: Text ('Letras ${_comentario.length}'),
                                labelText: 'Comentario',
                                helperText: 'Breve comentario hasta 300 caracteres',
                                icon: Icon(Icons.comment),
                                      ),
                                maxLength: 300,
                                textInputAction: TextInputAction.newline,
                                keyboardType: TextInputType.multiline,
                                maxLines: 5,
                                onChanged: (value){
                                        setState(() {
                                          _comentario = value;
                                        });
                                        
                                },

                                
                            ),
                                  ),


                      ],

                  )),

                ),
                SizedBox(height: 15),
              
                
               Padding(
                 padding: const EdgeInsets.all(8.0),
                 child: Row(
                          children: <Widget>[
                              
                              ClipRRect(
                                  borderRadius: BorderRadius.circular(10.0),
                                  child: RaisedButton(
                                    splashColor: Colors.yellow[200],
                                    color: Color(0xFF448aff),
                                    onPressed: () {
                                       Navigator.of(context).pop();
                                    },
                                    textColor: Colors.white,

                                    padding: const EdgeInsets.all(10.0),
                                    child:Text(
                                      'Cancelar',
                                      style: TextStyle(fontSize: 25)
                                    ),
                                  ),
                                ),

                              SizedBox(width: 20),
                              ClipRRect(
                                   borderRadius: BorderRadius.circular(10.0),
                                   child: RaisedButton(
                                     splashColor: Colors.yellow[200],
                                    color: Color(0xFF448aff),
                                    onPressed: _ratingStar == 0.0 ? null : () => _enviarComentario(context),
                                    textColor: Colors.white,
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      'Publicar',
                                      style: TextStyle(fontSize: 25)
                                    ),
                                ),
                              ),
                          ],


                            ),
               ),
              
                
                SizedBox(height: 2),
                Center(
                    child: Container(
                    padding: EdgeInsets.all(20.0),
                    child: _isFetching ? LinearProgressIndicator() : Text("La reseña será pública"),
                    ),
                ),

                ],
                
              ),
              
            ),
      ),

      ]
    );

  }

  void _enviarComentario(context) async{

    

    if (_isFetching)return;

    final isValid = _formKey.currentState.validate();
        if (isValid){
          setState(() {
              _isFetching =true;
            });
          final isResponse = await _apiService.storeComentario( context,
                                          articulo: widget.articulo.identificador,
                                          usuario: user['user_id'],
                                          puntuacion:_ratingStar.round(),
                                          comentario: _comentario,
                                          );
            setState(() {
              _isFetching =false;
            });

          if (isResponse){
                  _respuesta();
           
          }

          }

  }

  _headerComentario() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(5.0),
      child: Card(
        elevation: 15,
        color: Color(0xfffafafa).withOpacity(0.9),
        child: Column(
          children:<Widget>[

            ListTile(
              leading: FaIcon(FontAwesomeIcons.comments,color:Colors.black.withOpacity(0.6), size: 20,),
              title: Text(user  == null ? 'Cargando ..' : '${widget.articulo.tituloEs}',
              textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Color(0xff000a12))),
              subtitle: Text(user  == null ? 'Cargando ..' : '${widget.articulo.denominacionEnt}',
                      style: TextStyle(fontWeight: FontWeight.normal,fontSize: 15,color: Color(0xff000a12))),
               
            ),
            ListTile(
              leading: FaIcon(FontAwesomeIcons.userCircle,color:Colors.black.withOpacity(0.6), size: 20,),
              title: Text(user  == null ? 'Cargando ..' : '${user['user_surname']}, ${user['user_name']}',
              textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15,color: Color(0xff000a12))),
               
            ),

          ]
        ),
      ),
    );
  }


  _respuesta() async {
      await showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Text('Muchas Gracias por su valoracion'),
              actions: [
                new FlatButton(
                  child: new Text("Ver reseña"),
                  onPressed: () => Navigator.of(context).pop()
                ),
              ],
            ),
      );
       Navigator.pushReplacement(context,
        MaterialPageRoute(
        builder: (context) => ShowScan(widget.articulo.slug)));
    }


}


