class Comment {
  int identificador;
  int articulo;
  int usuario;
  String puntuacion;
  String detalle;
  String reaccion;
  String fechaCreacion;
  String fechaActualizacion;
  String fechaEliminacion;

  Comment({
    this.identificador,
    this.articulo,
    this.usuario,
    this.puntuacion,
    this.detalle,
    this.reaccion,
    this.fechaCreacion,
    this.fechaActualizacion,
    this.fechaEliminacion,
  });



  factory Comment.fromJson(Map<String, dynamic> json) {
    return  Comment(
        articulo  : json['articulo'],
        usuario   : json['usuario'],
        puntuacion: json['puntuacion'],
        detalle   : json['detalle'],
        reaccion : json['reaccion'],
        );
  }
}
