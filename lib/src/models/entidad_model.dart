
class Entidades{
  List<Entidad> items = new List();
  Entidades();

  Entidades.fromJsonList(List<dynamic> jsonList){
    if (jsonList == null) return;
    for (var item in jsonList) {
      final entidad = new Entidad.fromJson(item);
      items.add(entidad);
    }
  }

}

class Entidad {
    int identificador;
    int localidad;
    String denominacion;
    String ubicacion;
    int telefono;
    String correo;
    String direccionWeb;
    double longitud;
    double latitud;
    String imagen;
    DateTime fechaCreacion;
    DateTime fechaActualizacion;
    String fechaEliminacion;
    String denominacionL;

    Entidad({
        this.identificador,
        this.localidad,
        this.denominacion,
        this.ubicacion,
        this.telefono,
        this.correo,
        this.direccionWeb,
        this.longitud,
        this.latitud,
        this.imagen,
        this.fechaCreacion,
        this.fechaActualizacion,
        this.fechaEliminacion,
        this.denominacionL,
    });

    Entidad.fromJson(Map<String, dynamic> json) {
        identificador       = json["identificador"];
        localidad           = json["localidad"];
        denominacion        = json["denominacion"];
        ubicacion           = json["ubicacion"];
        telefono            = json["telefono"];
        correo              = json["correo"];
        direccionWeb        = json["direccionWeb"];
        longitud            = json["longitud"].toDouble();
        latitud             = json["latitud"].toDouble();
        imagen              = json["imagen"];
        fechaCreacion       = DateTime.parse(json["fechaCreacion"]);
        fechaActualizacion  = DateTime.parse(json["fechaActualizacion"]);
        fechaEliminacion    = json["fechaEliminacion"];
        denominacionL       = json["denominacionL"];

    }

  getImagen (){
    if (imagen == null){
      return 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSSSDAflI2W07ixtGLC1dxDGHkR5x9rek2BHlY3q07ZoxaTwv_d';
    } else {
      return imagen;
    }


  }



    
}
