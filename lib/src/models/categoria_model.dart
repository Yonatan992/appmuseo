class Category {
  int    identificador;
  String aliasEs;
  String aliasEn;
  String imagen;
  String fechaCreacion;

  Category({
    this.identificador,
    this.aliasEs,
    this.aliasEn,
    this.imagen,
    this.fechaCreacion,
    
  });

   factory Category.fromJson(Map<String, dynamic> json) {
    return  Category(
        identificador :json['identificador'],
        aliasEs       :json['aliasEs'],
        aliasEn       :json['aliasEn'],
        imagen        :json['imagen'],
        fechaCreacion :json['fechaCreacion'],
       
        );
  }

  
}

class Categories{
  List<Category> items = new List();
  Categories();

  Categories.fromJsonList(List<dynamic> jsonList){
    if (jsonList == null) return;
    for (var item in jsonList) {
      final categoria = new Category.fromJson(item);
      items.add(categoria);
    }
  }

}