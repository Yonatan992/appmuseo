class Articulos{
  List<Articulo> items = new List();
  Articulos();

  Articulos.fromJsonList(List<dynamic> jsonList){
    if (jsonList == null) return;
    for (var item in jsonList) {
      final articulo = new Articulo.fromJson(item);
      items.add(articulo);
    }
  }

}

class Comentarios{
  List<Comment> items = new List();
  Comentarios();

  Comentarios.fromJsonList(List<dynamic> jsonList){
    if (jsonList == null) return;
    for (var item in jsonList) {
      final comentario = new Comment.fromJson(item);
      items.add(comentario);
    }
  }

}


class Articulo {
  int identificador;
  int categoria;
  int entidad;
  String tituloEs;
  String tituloEn;
  String subtituloEs;
  String subtituloEn;
  String descripcionEs;
  String descripcionEn;
  String cuerpoEs;
  String cuerpoEn;
  String slug;
  String estado;
  String imagen;
  String video;
  String qr;
  String fechaCreacion;
  String fechaActualizacion;
  String fechaEliminacion;
  String denominacionCat;
  String denominacionCatEn;
  String denominacionEnt;
  final List<Comment> comments;

  Articulo({
    this.identificador,
    this.categoria,
    this.entidad,
    this.tituloEs,
    this.tituloEn,
    this.subtituloEs,
    this.subtituloEn,
    this.descripcionEs,
    this.descripcionEn,
    this.cuerpoEs,
    this.cuerpoEn,
    this.slug,
    this.estado,
    this.imagen,
    this.video,
    this.qr,
    this.fechaCreacion,
    this.fechaActualizacion,
    this.fechaEliminacion,
    this.denominacionCat,
    this.denominacionCatEn,
    this.denominacionEnt,
    this.comments,
  });


factory Articulo.fromJson(Map <String,dynamic> json) {
   var list = json['comments'] as List;
    print(list.runtimeType);
    List<Comment> commentList = list.map((i) => Comment.fromJson(i)).toList();
    
  return  Articulo(
    identificador: json['identificador'],
    categoria  : json['categoria'],
    entidad  : json['entidad'],
    tituloEs : json['tituloEs'],
    tituloEn : json['tituloEn'],
    subtituloEs  : json['subtituloEs'],
    subtituloEn  : json['subtituloEn'],
    descripcionEs  : json['descripcionEs'],
    descripcionEn  : json['descripcionEn'],
    cuerpoEs : json['cuerpoEs'],
    cuerpoEn : json['cuerpoEn'],
    slug : json['slug'],
    estado : json['estado'],
    imagen : json['imagen'],
    video  : json['video'],
    qr : json['qr'],
    fechaCreacion  : json['fechaCreacion'],
    fechaActualizacion : json['fechaActualizacion'],
    fechaEliminacion : json['fechaEliminacion'],
    denominacionCat  : json['denominacionCat'],
    denominacionCatEn  : json['denominacionCatEn'],
    denominacionEnt  : json['denominacionEnt'],
    comments : commentList,

  );

}


}

class Comment {
  int    idComment;
  String valoracion;
  String comentario;
  int    idUser;
  String apellido;
  String nombre;
  String respuesta;
  String fecha;

  Comment({
    this.idComment,
    this.valoracion,
    this.comentario,
    this.idUser,
    this.apellido,
    this.nombre,
    this.respuesta,
    this.fecha,
  });

   factory Comment.fromJson(Map<String, dynamic> json) {
    return  Comment(
        idComment  :json['idComment'],
        valoracion :json['valoracion'],
        comentario :json['comentario'],
        idUser     :json['idUser'],
        apellido   :json['apellido'],
        nombre     :json['nombre'],
        respuesta  :json['respuesta'],
        fecha      :json['fecha'],
        );
  }
}


