import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';


class Session {

  final key = "SESSION";

  final storage = new FlutterSecureStorage();

  set (int userId, String userName, String userSurname,String userVerify, String token,String typeToken,String expires) async {
    final data = {
      "user_id"     :userId,
      "user_name"   :userName,
      "user_surname":userSurname,
      "user_verify" :userVerify,      
      "access_token":token,
      "token_type"  :typeToken,
      "expires_at"  :expires,
      "createdAt"   :DateTime.now().toString()
    };
    //lo guardamos como string de manera segura en el dispositivo
    await storage.write(key: key, value: jsonEncode(data));

  }

  getSession()async{
    // Read value 
    final result = await storage.read(key: key);
    if(result != null){
      return jsonDecode(result);
    }

    return null;

  }

  onClosed() async{
    await storage.deleteAll();
  }


}