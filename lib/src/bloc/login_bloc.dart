
import 'dart:async';

import 'package:lectorqr/src/bloc/validators.dart';
import 'package:rxdart/subjects.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc with Validators{

  final _emailController    = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();

  //recueprar los datos del Stream
  Stream <String> get emailStream    =>_emailController.stream.transform(validarEmail);
  Stream <String> get passwordStream =>_passwordController.stream.transform(validarPassword);

  Stream<bool> get formValidStream => 
      Observable.combineLatest2(emailStream, passwordStream,(e,p) => true);


  //insertar valores al Stream

  Function (String) get changeEmail    => _emailController.sink.add;

  Function (String) get changePassword => _passwordController.sink.add;

  // Obterner el ultimo valor ingresado a los streams
  String get email => _emailController.value;
  String get password => _passwordController.value;

  dispose(){
    _emailController?.close();
    _passwordController?.close();

  }
}