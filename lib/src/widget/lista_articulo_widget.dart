import 'package:flutter/material.dart';
import 'package:lectorqr/src/models/articulos_model.dart';

class ListaAticulos extends StatelessWidget {

final  List<Articulo> articulos;

  const ListaAticulos({this.articulos});

 

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: this.articulos.length,
      itemBuilder: (BuildContext contex, int index){
        Articulo articulo = articulos[index];
        return Text(articulo.tituloEs);

      });
  }
}