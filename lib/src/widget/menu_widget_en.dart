import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';

class MenuEnWidget extends StatelessWidget {
  const MenuEnWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return  FadeInLeft(
        child: Drawer(
          child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              margin: EdgeInsets.zero,
              padding: EdgeInsets.zero,
              decoration: BoxDecoration(
                  image: DecorationImage(
                          image:AssetImage('assets/menu-img.jpg'),
                          fit: BoxFit.cover
                          )
               ),
              child: Stack(children: <Widget>[
                  Positioned(
                      bottom: 12.0,
                      left: 16.0,
                      child: Text("Menu",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 25.0,
                              fontWeight: FontWeight.w500))),
                ])
            ),
            ListTile(
              leading: Icon(Icons.pages, color: Colors.blue),
              title: Text('Home'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'Scroll');
              },
            ),
            Divider(
                color: Colors.black,
                height: size.height * 0.020,),
            ListTile(
              leading: Icon(Icons.perm_identity,color: Colors.blue),
              title: Text('Log in'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'loginEn');
              },
            ),
            ListTile(
              leading: Icon(Icons.menu, color: Colors.blue),
              title: Text('Navegation menu'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'buttons');
              },
            ),
            
            ListTile(
              leading: Icon(Icons.line_style, color: Colors.blue),
              title: Text('Navegation menu Español'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'botones');
              },
            ),
            Divider(
                color: Colors.black87,
                height: size.height * 0.010,),
            ListTile(
              leading: Icon(Icons.account_balance, color: Colors.blue),
              title: Text('Museums'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'museums');
              },
            ),
            ListTile(
              leading: Icon(Icons.library_books, color: Colors.blue),
              title: Text('Catalogs'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'articles');
              },
            ),
            ListTile(
              leading: Icon(Icons.category, color: Colors.blue),
              title: Text('Categories'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'Categories');
              },
            ),

            ListTile(
              leading: Icon(Icons.star, color: Colors.blue),
              title: Text('Top rated'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'topRated');
              },
            ),

            ListTile(
              leading: Icon(Icons.developer_board, color: Colors.blue),
              title: Text('Scan'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'scanEn');
              },
            ),

          ],
        ),
      ),
    );
  }
}