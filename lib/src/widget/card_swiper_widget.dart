import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:lectorqr/src/models/entidad_model.dart';

class CardSwiper extends StatelessWidget {
  final List<Entidad> entidades;
  CardSwiper({@required this.entidades});

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.only(top: 2.0),
      child: new Swiper(
        layout: SwiperLayout.STACK,
        itemWidth: _screenSize.width * 0.5, // 70 % DEL ANCHO DEL DISPOSITIVO
        itemHeight: _screenSize.height * 0.3, //la mistad del dispositivo
        itemBuilder: (BuildContext context, int index) {
          return ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
             image: NetworkImage(entidades[index].getImagen()),
             placeholder: AssetImage('assets/carga.gif'),
             fit: BoxFit.cover,
            ),
          );
        },
        itemCount: 4,
        // pagination: new SwiperPagination(),
        //control: new SwiperControl(),
      ),
    );
  }
}
