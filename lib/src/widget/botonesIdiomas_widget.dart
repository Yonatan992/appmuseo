import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BotonesIdiomas extends StatelessWidget {
   @required final String ruta;
  @required final String image;
  final IconData icon;
  @required final String texto;
  final Color color1;
  final Color color2;
  @required final Function onPress;

  const BotonesIdiomas({
    this.ruta,
    this.image,
    this.icon = FontAwesomeIcons.circle, 
    this.texto, 
    this.color1 = Colors.grey, 
    this.color2 = Colors.blueGrey, 
    this.onPress});

  

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, this.ruta),
      child: Stack(
        children:<Widget>[
          _BotonIdiomaBackground(this.color1,this.color2),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 140, width: 40,),
              Image(
                image: AssetImage(this.image),
                width: 80,
                height: 80,
                fit: BoxFit.cover,
              ),
               SizedBox( width: 40,),
              Expanded(child: Text(this.texto,style: TextStyle(color:Colors.white, fontSize:30,fontWeight: FontWeight.bold))),
              FaIcon(this.icon, color:Colors.white, size: 40,),
              SizedBox( width: 40,),


            ],
          )

        ]
      ),
    );
  }
}

class _BotonIdiomaBackground extends StatelessWidget {
   final Color color1;
  final Color color2;

  const _BotonIdiomaBackground(this.color1, this.color2);

  @override
  Widget build(BuildContext context) {
    return Container(

      child: ClipRRect(
        // Corta los bordes de mas con el ClipRRect corta lo que sobresale de su hijo Stack
        borderRadius: BorderRadius.circular(15),
        child: Stack(
          children: <Widget>[
            Positioned(
              right: -20,
              top: -20,
              child:FaIcon(FontAwesomeIcons.globe, size: 130, color: Colors.white.withOpacity(0.3) )
              ),
          ]
        ),
      ),



      width: double.infinity,
      height: 100,
      margin: EdgeInsets.all(20),
      
      decoration: BoxDecoration(
        color:Colors.red,
        boxShadow: <BoxShadow>[
          BoxShadow(color:Colors.black.withOpacity(0.5),offset:Offset(4,6),blurRadius: 15),
        ],
        borderRadius: BorderRadius.circular(15),
        gradient: LinearGradient(
          colors: <Color>[
            this.color1,
            this.color2,
          ])

      ),
    );
  }
}