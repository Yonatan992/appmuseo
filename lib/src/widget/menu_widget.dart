import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';

class MenuWidget extends StatelessWidget {
  const MenuWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return  FadeInLeft(
        child: Drawer(
          child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              margin: EdgeInsets.zero,
              padding: EdgeInsets.zero,
              decoration: BoxDecoration(
                  image: DecorationImage(
                          image:AssetImage('assets/menu-img.jpg'),
                          fit: BoxFit.cover
                          )
               ),
              child: Stack(children: <Widget>[
                  Positioned(
                      bottom: 12.0,
                      left: 16.0,
                      child: Text("Menú",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 25.0,
                              fontWeight: FontWeight.w500))),
                ])
            ),
            ListTile(
              leading: Icon(Icons.pages, color: Colors.blue),
              title: Text('Principal'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'Scroll');
              },
            ),
            Divider(
                color: Colors.black,
                height: size.height * 0.020,),
            ListTile(
              leading: Icon(Icons.perm_identity,color: Colors.blue),
              title: Text('Iniciar sesión'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'loginEs');
              },
            ),
            ListTile(
              leading: Icon(Icons.menu, color: Colors.blue),
              title: Text('Menú de Navegación'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'botones');
              },
            ),
            
            ListTile(
              leading: Icon(Icons.line_style, color: Colors.blue),
              title: Text('Menú de Navegación Inglés'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'buttons');
              },
            ),
            Divider(
                color: Colors.black87,
                height: size.height * 0.010,),
            ListTile(
              leading: Icon(Icons.account_balance, color: Colors.blue),
              title: Text('Museos'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'entidades');
              },
            ),
            ListTile(
              leading: Icon(Icons.library_books, color: Colors.blue),
              title: Text('Catálogos'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'articulos');
              },
            ),
            ListTile(
              leading: Icon(Icons.category, color: Colors.blue),
              title: Text('Categorías'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'Categorias');
              },
            ),

            ListTile(
              leading: Icon(Icons.star, color: Colors.blue),
              title: Text('Mejores valorados'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'mejoresVal');
              },
            ),

            ListTile(
              leading: Icon(Icons.developer_board, color: Colors.blue),
              title: Text('Scan'),
              onTap: (){
                //Navigator.pop(context);
                Navigator.pushReplacementNamed(context,'Scan');
              },
            ),

          ],
        ),
      ),
    );
  }
}