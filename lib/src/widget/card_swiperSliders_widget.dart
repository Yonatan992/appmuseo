import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class SwiperSliders extends StatefulWidget {
  
  @override
  _SwiperSlidersState createState() => _SwiperSlidersState();
}

class _SwiperSlidersState extends State<SwiperSliders> {
  
  var imagenes = ['assets/imgSliders/slide-0.jpg','assets/imgSliders/slide-1.png','assets/imgSliders/slide-2.png',
                  'assets/imgSliders/slide-3.png','assets/imgSliders/slide-4.png','assets/imgSliders/slide-5.png',
                  'assets/imgSliders/slide-6.png'];

  @override
  Widget build(BuildContext context) {
    return  Container ( 
      width: double.infinity,
      height: 300,
      child: Swiper(
        itemBuilder: (BuildContext context,int index){
          return  ClipRRect( 
            borderRadius: BorderRadius.circular(15.0),
            child: Image(
                image:AssetImage(imagenes[index]),
                fit: BoxFit.fill,
                
                ),
          );
        },
        itemCount: imagenes.length,
        itemWidth:350,
        itemHeight: 450,
        layout: SwiperLayout.TINDER,
        pagination: new SwiperPagination(),
        control: new SwiperControl(),
      ),
      
    );
  }
}