import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


//import 'package:lectorqr/src/pages/home_page.dart';

import 'package:lectorqr/src/pages/scroll_page.dart';
import 'package:lectorqr/src/providers/DirectionsProvider.dart';
import 'package:lectorqr/src/routes/routes.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
        
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
      ]);
    //Provider es una clase que se crea para manejar o manipular la informacion  o el estado de la misma del login (es lo que recomienda flutter)
    return MultiProvider(
      providers: [
        //ChangeNotifierProvider(create: (_)=> new ArticleService(),),
        ChangeNotifierProvider(create: (_)=> new DirectionProvider(),),
        
      ],
      child:  MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Lector QR',
      initialRoute: 'scroll',
      routes: getApplicationRoutes(),
      onGenerateRoute: (RouteSettings settings) {
        print('Ruta llamada: ${settings.name}');
        return MaterialPageRoute(
            builder: (BuildContext context) => ScrollPage());
      },
      theme: ThemeData(primaryColor: Colors.blue),
    ),
    );
  }
}
